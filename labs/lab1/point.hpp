/* 
   Conner Becker, Samuel Congdon, Chance Ronning
   CSCI 441: Graphics
   Lab 1: Software Rasterization
   January 15, 2018
   
   This class handles points, handled by triangle.hpp. Points consists of their
   x and y coordinates, as well as their rgb color values. 
*/

class Point {
public:
    float x;
    float y;
    float r;
    float g;
    float b;

    // Constructor
    Point();
    Point(float xc, float yc) : x(xc), y(yc) {
        std::cout << "Created point with x,y: " << x << "," << y << std::endl;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    //~Point() {
    //    std::cout << "Point: " << x << "," << y << " destructor" << std::endl;
    //}
};

Point::Point() {}
std::ostream& operator<<(std::ostream& os, const Point& pt) {
	os << pt.x << "," << pt.y << ":" << pt.r << "," << pt.g << "," << pt.b;
	return os;
}
