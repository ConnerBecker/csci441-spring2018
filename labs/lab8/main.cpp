#include <iostream>
#include <vector>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"
#include "Primitives.cpp"

#define WIDTH 640
#define HEIGHT 480
#define DIST 2

std::vector<Ray> rays;

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

void calculateU(float* w, float* up, float* buf) {
    normalize(w, 3);
    float u[3] = { up[1]*w[2] - up[2]*w[1],
                   up[2]*w[0] - up[0]*w[2],
                   up[0]*w[1] - up[1]*w[0] };
    normalize(u,3);
    buf[0] = u[0];
    buf[1] = u[1];
    buf[2] = u[2];
}

void calculateV(float* w, float* u, float* buf) {
    normalize(w, 3);
    float v[3] = { w[1]*u[2] - w[2]*u[1],
                   w[2]*u[0] - w[0]*u[2],
                   w[0]*u[1] - w[1]*u[0] };
    normalize(v,3);
    buf[0] = v[0];
    buf[1] = v[1];
    buf[2] = v[2];
}

void colorBitmap(bitmap_image* img, rgb_t* color) {
    for (int x = 0; x < WIDTH; x++) {
        for (int y = 0; y < HEIGHT; y++) {
            img->set_pixel(x,y,*color);
        }
    }
}

//this function iterates over every pixel in an image an creates a ray for it
// Ray Params:
// l,r = left, right
// b,t = top, bottom
// i,j = pixel (x,y)
// nX = image width
// nY = image height
// w,u,v = w,u,v float matrices
// cam = camera origin
// dist = distance used in perspective viewing
void createRays(Viewport view, float* w, float* u, float* v, float* cam) {
    float l = view.min[0];
    float r = view.max[0];
    float b = view.min[1];
    float t = view.max[1];
    for (int x = 0; x < WIDTH; x++) {
        for (int y = 0; y < HEIGHT; y++) {
            rays.push_back(Ray(l,r,b,t,x,y,WIDTH, HEIGHT, w, u, v, cam, DIST) );
        }
    }
}

// this function renders and image using the orthographic attributes of a ray
void renderOrtho(bitmap_image& image, const std::vector<Sphere>& world) {
    // ortho
    for (int i = 0; i < rays.size(); i++) {
        float smallest_t = 99999;
        for (int w = 0; w < world.size(); w++){
            float center[3] = {world[w].center[0],
                               world[w].center[1],
                               world[w].center[2] };
            float rad = world[w].radius;
            float t_val = rays[i].intersectSphere(center, rad);
            if (t_val >= 0 && t_val < smallest_t) {
                smallest_t = t_val;
                rgb_t color = make_colour(world[w].color[0],world[w].color[1],world[w].color[2]);
                image.set_pixel(rays[i].x,rays[i].y,color);
            }
        }
    }
}

// this function renders an image using the perspective attributes of a ray
void renderPersp(bitmap_image& image, const std::vector<Sphere>& world) {
    // persp
    for (int i = 0; i < rays.size(); i++) {
        float smallest_t = 99999;
        for (int w = 0; w < world.size(); w++){
            float center[3] = {world[w].center[0],
                               world[w].center[1],
                               world[w].center[2] };
            float rad = world[w].radius;
            float t_val = rays[i].intersectSpherePersp(center, rad);
            if (t_val >= 0 && t_val < smallest_t) {
                smallest_t = t_val;
                rgb_t color = make_colour(world[w].color[0],world[w].color[1],world[w].color[2]);
                image.set_pixel(rays[i].x,rays[i].y,color);
            }
        }
    }
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(WIDTH, HEIGHT);
    rgb_t color = make_colour(85, 210, 100);
    colorBitmap(&image, &color);
    
    bitmap_image image2(WIDTH, HEIGHT);
    rgb_t color2 = make_colour(85, 210, 100);
    colorBitmap(&image2, &color2);
    
    // create View
    Viewport view(glm::vec2(-5,-5),glm::vec2(5,5));
    float origin[3] = {0.0f,0.0f,0.0f};
    float gaze[3]   = {0.0f,0.0f,1.0f};
    float up[3]     = {0.0f,1.0f,0.0f};
    float u[3];
    float v[3];
    float w[3] = {-gaze[0],
                  -gaze[1],
                  -gaze[2]};
    calculateU(w,up,u);
    calculateV(w,u,v);
    createRays(view,w,u,v,origin);
    
    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(  0, 0, 4), 1, glm::vec3(255,255,0)),
        Sphere(glm::vec3( -1, 0, 8), 2, glm::vec3(0,255,255)),
        Sphere(glm::vec3( -2, 0, 12), 3, glm::vec3(255,0,255)),
    };

    // render the world
    renderOrtho(image, world);
    renderPersp(image2, world);
    
    image.save_image("ortho.bmp");
    std::cout << "Success" << std::endl;
    
    image2.save_image("persp.bmp");
    std::cout << "Success" << std::endl;
}


