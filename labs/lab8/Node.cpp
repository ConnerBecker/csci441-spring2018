#include <iostream>
#include <vector>
#include <string>
#include <math.h>

#define PI 3.141592654

class ShapeNode {
    public:
    std::vector<float> points;
    float scale[16]     = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    float rotateX[16]   = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    float rotateY[16]   = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    float rotateZ[16]   = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    float translate[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    float model[16]     = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    ShapeNode* next;
    ShapeNode* previous;
    // Constructor
    ShapeNode();
    ShapeNode(ShapeNode*, ShapeNode*);
    void drawShape();
    void addVertices(std::vector<float>);
    void addVertices(float*, int);
    void setNext(ShapeNode*);
    void setPrevious(ShapeNode*);
    void changeColor();
    void changeColor(float, float, float);
    void changeSize(float, float, float);
    void changeXRotation(float);
    void changeYRotation(float);
    void changeZRotation(float);
    void changePosition(float, float, float);
    void createModel();
    // Destructor - called when an object goes out of scope or is destroyed
    ~ShapeNode() {
        std::cout << "Destroyed Node" << std::endl;
    }
};

ShapeNode::ShapeNode() {
    std::cout << "Created empty node." << std::endl;
}

ShapeNode::ShapeNode(ShapeNode* x, ShapeNode* y) {
    next = x;
    previous = y;
    std::cout << "Created linked noded." << std::endl;
}

void print4x4(float* buf) {
    std::cout << "\n";
    std::cout << buf[0] <<", "<< buf[1] <<", "<< buf[2] <<", "<< buf[3] <<"\n";
    std::cout << buf[4] <<", "<< buf[5] <<", "<< buf[6] <<", "<< buf[7] <<"\n";
    std::cout << buf[8] <<", "<< buf[9] <<", "<< buf[10] <<", "<< buf[11] <<"\n";
    std::cout << buf[12] <<", "<< buf[13] <<", "<< buf[14] <<", "<< buf[15] <<"\n"<<std::endl;
}

void setIdentity4x4(float* buf) {
    for (int i = 0; i < 16; i ++) {
        buf[i] = 0;
    }
    buf[0] = 1.0f;
    buf[5] = 1.0f;
    buf[10] = 1.0f;
    buf[15] = 1.0f;
}

void transpose4x4(float* buf, float* m) {
    for (int i = 0; i < 16; i++) {
        buf[i] = m[i%4*4+(i/4)];
    }
}

void mult4x4(float* buf, float* m1, float* m2) {
    float temp[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
    float left[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
    float right[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
    
    transpose4x4(left, m1);
    transpose4x4(right, m2);
    
    for (int row = 0; row < 4; row++) {
        for (int column = 0; column < 4; column++) {
            
            float total=0;
            total = total + left[row*4+0]*right[column+0];
            total = total + left[row*4+1]*right[column+4];
            total = total + left[row*4+2]*right[column+8];
            total = total + left[row*4+3]*right[column+12];
            temp[row*4+column] = total;
        }
    }
    transpose4x4(buf, temp);
}

bool inverse_transpose(float* m, float* invOut) {
    /* Function to calculate the transposed inverse of a 3x3 matrix from the passed in 4x4 model
     * matrix. Only the top left 3x3 area is used in order to ignore the effects of translation on
     * the normal matrix. Returns false if the determinant is found to be zero, otherwise returns
     * true. */
    float inv[9], det;
    int i;

    inv[0] = m[5]*m[10] - m[9]*m[6];
    inv[3] = -(m[1]*m[10] - m[2]*m[9]);
    inv[6] =  m[1]*m[6] - m[2]*m[5];
    inv[1] = -(m[4]*m[10] - m[6]*m[8]);
    inv[4] = m[0]*m[10] - m[2]*m[8];
    inv[7] = -(m[0]*m[6] - m[4]*m[2]);
    inv[2] =  m[4]*m[9] - m[8]*m[5];
    inv[5] = -(m[0]*m[9] - m[8]*m[1]);
    inv[8] =  m[0]*m[5] - m[4]*m[1];

    det = m[0]*m[5]*m[10] + m[4]*m[9]*m[2] + m[8]*m[1]*m[6] - m[8]*m[5]*m[2] - m[4]*m[1]*m[10] - m[0]*m[9]*m[6];

    if (det == 0) {
        return false;
    }

    det = 1/det;

    for (i = 0; i < 9; i++) {
        invOut[i] = inv[i] * det;
    }
    
    return true;
}

void ShapeNode::createModel() {
    setIdentity4x4(model);
    mult4x4(model, model, translate);
    mult4x4(model, model, scale);
    mult4x4(model, model, rotateX);
    mult4x4(model, model, rotateY);
    mult4x4(model, model, rotateZ);
}

void ShapeNode::changeColor() {
    float r, g, b;
    char seperator;
    std::cout << "Please enter new RGB in form r,g,b." << std::endl;
    std::cin >> r >> seperator >> g >> seperator >> b;
    for(int i = 0; i < points.size(); i+=5) {
        points[i+2] = r;
        points[i+3] = g;
        points[i+4] = b;
    }
    registerShape();
}

void ShapeNode::changeColor(float r, float g, float b) {
    for(int i = 0; i < points.size(); i+=9) {
        points[i+3] = r;
        points[i+4] = g;
        points[i+5] = b;
    }
    registerShape();
}

void ShapeNode::changePosition(float x, float y, float z) {

    translate[12] += x;
    translate[13] += y;
    translate[14] += z;
    translate[15] = 1;
}

void ShapeNode::changeSize(float x, float y, float z) {

    scale[0] = x;
    scale[5] = y;
    scale[10] = z;
}

void ShapeNode::changeXRotation(float a) {
    float angle = PI*a/180.0f;
    rotateX[5] = cos(angle);
    rotateX[9] = -sin(angle);
    rotateX[6] = sin(angle);
    rotateX[10] = cos(angle);
}

void ShapeNode::changeYRotation(float a) {
    float angle = PI*a/180.0f;
    rotateY[0] = cos(angle);
    rotateY[8] = sin(angle);
    rotateY[2] = -sin(angle);
    rotateY[10] = cos(angle);
}

void ShapeNode::changeZRotation(float a) {
    float angle = PI*a/180.0f;
    rotateZ[0] = cos(angle);
    rotateZ[4] = -sin(angle);
    rotateZ[1] = sin(angle);
    rotateZ[5] = cos(angle);
}

void ShapeNode::setNext(ShapeNode* n) {
    next = n;
}

void ShapeNode::setPrevious(ShapeNode* p) {
    previous = p;
}

void ShapeNode::setShader(GLuint s) {
    this->shader = s;
}

void ShapeNode::addVertices(std::vector<float> data) {
    for(int i = 0; i < data.size(); i++) {
        points.push_back(data[i]);
    }
}

void ShapeNode::addVertices(float* data, int count) {
    for(int i = 0; i < count; i++) {
        points.push_back(data[i]);
    }
}

class ShapeNodeList {
    public:
    ShapeNode* first;
    ShapeNode* last;
    float perspective[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    float camera[16]      = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    float light[3] = {0,1.25,1};
    float eye[3] = {0,0,0};
    void append(ShapeNode*);    
    void setFirst(ShapeNode*);
    void primeFirst(ShapeNode*);
    void addNewShape(std::vector<float>);
    bool isNotEmpty();
    void changePerspective(float, float, float, float);
    void changePerspective(float, float, float, float, float ,float);
    void face_camera(float*, float*, float*, float, float, float*);
    void changeView();
    void addLight(float*);
    void moveLight(float, float, float);
    void addEye(float*);
    ShapeNodeList();
    private:
    void normalize(float*, int);
    bool primeCalled = false;
};

void rotateX1x3(float* buf, float angle) {
    angle = PI*angle/180.0f;
    buf[1] = buf[1] * cos(angle)  + buf[2] * sin(angle);
    buf[2] = buf[1] * -sin(angle) + buf[2] * cos(angle);
}

void rotateY1x3(float* buf, float angle) {
    angle = PI*angle/180.0f;
    buf[0] = buf[0] * cos(angle)  + buf[2] * -sin(angle);
    buf[2] = buf[0] * sin(angle) + buf[2] * cos(angle);
}

void rotateZ1x3(float* buf, float angle) {
    angle = PI*angle/180.0f;
    buf[0] = buf[0] * cos(angle)  + buf[1] * sin(angle);
    buf[1] = buf[0] * -sin(angle) + buf[1] * cos(angle);
}

ShapeNodeList::ShapeNodeList() {
    std::cout << "New List created" << std::endl;
}

void ShapeNodeList::setFirst(ShapeNode* n) {
    ShapeNode* temp = first->next;
    first = n;
    first->next = temp;
}

bool ShapeNodeList::isNotEmpty() {
    return primeCalled;
}

void ShapeNodeList::primeFirst(ShapeNode* n) {
    primeCalled = true;
    first = n;
    last = n;
    n->next = n;
    n->previous = n;
}

void ShapeNodeList::append(ShapeNode* n) {
    if(primeCalled){
        last->next = n;
        n->previous = last;
        last = n;
        n->next = first;
    }
    else{
        primeFirst(n);
    }
}

void ShapeNodeList::addNewShape(std::vector<float> verts){
    ShapeNode* newShape = new ShapeNode();
    newShape->addVertices(verts);
    std::cout << "Flag appending" << std::endl;
    append(newShape);
    std::cout << "Flag finished appending" << std::endl;
}

void ShapeNodeList::normalize(float* v, int sz) {
    float total = 0;
    for (int i = 0; i < sz; i++) {
        total = total + pow(v[i],2);
    }
    total = sqrt(total);
    for (int i = 0; i < sz; i++) {
        v[i] = v[i]/total;
    }
}

void ShapeNodeList::changePerspective(float fov, float ratio, 
                                      float n, float f) {

        float view_angle = fov*PI/180.0;
        
        perspective[0] = 1 / (ratio*tan(view_angle));
        perspective[5] = 1 / (tan(view_angle));
        perspective[10] = -(f+n) / (f-n);
        perspective[14] = (-2*f*n) / (f-n);
        perspective[11] = -1;
        perspective[15] = 0;
        
}

void ShapeNodeList::changePerspective(float l, float r, 
                                      float b, float t, 
                                      float n, float f) {
                
        
        perspective[0] = 2/(r-l);
        perspective[1] = 0;
        perspective[2] = 0;
        perspective[3] = (r+l)/(r-l);
        perspective[4] = 0;
        perspective[5] = 2/(t-b);
        perspective[6] = 0;
        perspective[7] = -((t+b)/(t-b));
        perspective[8] = 0;
        perspective[9] = 0;
        perspective[10] = -2/(f-n);
        perspective[11] = -((f+n)/(f-n));
        perspective[12] = 0;
        perspective[13] = 0;
        perspective[14] = 0;
        perspective[15] = 1;        
}

void ShapeNodeList::changeView() {
    viewMode = !viewMode;
}

/* this function uses the unit circle to aim the camera in a circle around
the player. When the orthographic display is used the camera is instead set
higher above the car, it faces striaght down, and the orthomatrix is used*/
void ShapeNodeList::face_camera(float* cam, float* targ, float* up, 
                                float angleX, float angleY, float* trans) {
                        
    angleY = PI*(angleY-90)/180.0;
    angleX = PI*(angleX-90)/180.0;
    
    //float oppositeXaxis = sin(angleX);
    //float adjacentXaxis = cos(angleX);
    float oppositeYaxis = sin(angleY);
    float adjacentYaxis = cos(angleY);
    if (viewMode) {
        cam[0] = trans[0];
        cam[1] = trans[1];
        cam[2] = trans[2];
        
        targ[0] = trans[0] + adjacentYaxis;
        targ[1] = trans[1];
        targ[2] = trans[2] - oppositeYaxis;
    } else {
        cam[0] = trans[0];
        cam[1] = trans[1]-4;
        cam[2] = trans[2];
        
        targ[0] = trans[0];
        targ[1] = trans[1];
        targ[2] = trans[2];
    }
    float w[3] = { targ[0] - cam[0], 
                   targ[1] - cam[1], 
                   targ[2] - cam[2] };
    normalize(w, 3);
    float u[3] = { up[1]*w[2] - up[2]*w[1],
                   up[2]*w[0] - up[0]*w[2],
                   up[0]*w[1] - up[1]*w[0] };
    normalize(u,3);
    float v[3] = { w[1]*u[2] - w[2]*u[1],
                   w[2]*u[0] - w[0]*u[2],
                   w[0]*u[1] - w[1]*u[0] };
    camera[0] = u[0];
    camera[1] = v[0];
    camera[2] = w[0];
    camera[3] = 0;
    camera[4] = u[1];
    camera[5] = v[1];
    camera[6] = w[1];
    camera[7] = 0;
    camera[8] = u[2];
    camera[9] = v[2];
    camera[10] = w[2];
    camera[11] = 0;
    camera[12] = -1 * (cam[0]*u[0]) - (cam[1]*u[1]) - (cam[2]*u[2]);
    camera[13] = -1 * (cam[0]*v[0]) - (cam[1]*v[1]) - (cam[2]*v[2]);
    camera[14] = -1 * (cam[0]*w[0]) - (cam[1]*w[1]) - (cam[2]*w[2]);
    camera[15] = 1;
}

void ShapeNodeList::addLight(float* l) {
    light[0] = l[0];
    light[1] = l[1];
    light[2] = l[2];
}

void ShapeNodeList::moveLight(float x, float y, float z) {
    light[0] = x;
    light[1] = y;
    light[2] = z;
}

void ShapeNodeList::addEye(float* e) {
    eye[0] = e[0];
    eye[1] = e[1];
    eye[2] = e[2];
}
