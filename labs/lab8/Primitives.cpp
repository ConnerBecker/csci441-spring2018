#include <vector>
#include <math.h>
#include <cstdlib>

#define PI 3.141592654

void loadIdentity4x4(float* buffer) {
    for(int i = 0; i < 16; i++) {
        buffer[i] = 0.0f;
    }
    buffer[0] = 1.0f;
    buffer[5] = 1.0f;
    buffer[10] = 1.0f;
    buffer[15] = 1.0f;
}

void normalize(float* v, int sz) {
    float total = 0;
    for (int i = 0; i < sz; i++) {
        total = total + pow(v[i],2);
    }
    total = sqrt(total);
    for (int i = 0; i < sz; i++) {
        v[i] = v[i]/total;
    }
}

void rotateX(float angle, float* buffer) {
    buffer[0] = 1.0f;
    buffer[1] = 0.0f;
    buffer[2] = 0.0f;
    buffer[3] = 0.0f;
    buffer[4] = 0.0f;
    buffer[5] = (float) cos(angle);
    buffer[6] = (float) -sin(angle);
    buffer[7] = 0.0f;
    buffer[8] = 0.0f;
    buffer[9] = (float) sin(angle);
    buffer[10] = (float) cos(angle);
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void rotateY(float angle, float* buffer) {
    buffer[0] = (float) cos(angle);
    buffer[1] = 0.0f;
    buffer[2] = (float) sin(angle);
    buffer[3] = 0.0f;
    buffer[4] = 0.0f;
    buffer[5] = 1.0f;
    buffer[6] = 0.0f;
    buffer[7] = 0.0f;
    buffer[8] = (float) -sin(angle);
    buffer[9] = 0.0f;
    buffer[10] = (float) cos(angle);
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void rotateZ(float angle, float* buffer) {
    buffer[0] = (float) cos(angle);
    buffer[1] = (float) -sin(angle);
    buffer[2] = 0.0f;
    buffer[3] = 0.0f;
    buffer[4] = (float) sin(angle);
    buffer[5] = (float) cos(angle);
    buffer[6] = 0.0f;
    buffer[7] = 0.0f;
    buffer[8] = 0.0f;
    buffer[9] = 0.0f;
    buffer[10] = 1.0f;
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void multMatrix4x4(float* b2, float* b1) {
    float temp[16];
    for(int r = 0; r < 4; r++) {
        for(int c = 0; c < 4; c++) {
            temp[r*4+c] = b1[r*4]*b2[c] + b1[r*4+1]*b2[c+4] + b1[r*4+2]*b2[c+8] + b1[r*4+3]*b2[c+12];
        }
    }
    for(int i = 0; i < 16; i++) {
        b2[i] = temp[i];
    }
}

class Ray {
    public:
    float origin[3];
    float direction[3];
    float origin_persp[3];
    float direction_persp[3];
    float x;
    float y;

    // Constructor
    Ray();
    Ray(float, float, float, float, 
        float, float, float, float, 
        float*, float*, float*, float*, float d);
    void setOrigin(float*);
    void setOriginPersp(float*);
    void setDirection(float*);
    void calculateOriginOrtho(float, float, float*, float*, float*);
    void calculateDirectionPersp(float, float, float, float*, float*, float*);
    float intersectSphere(float*, float);
    float intersectSpherePersp(float*, float);
    // Destructor - called when an object goes out of scope or is destroyed
    ~Ray() {
        //std::cout << "Destroyed Ray" << std::endl;
    }
};

Ray::Ray() {
    // created ray
}

// this constructor creates a ray and populates both its orthographic viewing
// attributes as well as the perspective viewing attributes.
Ray::Ray(float l, float r, float b, float t, 
         float i, float j, float nX, float nY,
         float* w, float* u, float* v, float* cam, float d) {
    x = i;
    y = j;
    float uR = l + (r - l) * (i + 0.5f)/nX;
    float vR = t + (b - t) * (j + 0.5f)/nY;
    
    setDirection(w);                                // used for orthographic
    setOriginPersp(cam);                            // used for perspective
    calculateOriginOrtho(uR, vR, u, v, cam);        // used for orthographic
    calculateDirectionPersp(uR, vR, d, u, v, w);    // used for perspective
}

void Ray::setOrigin(float* o) {
    origin[0] = o[0]; origin[1] = o[1]; origin[2] = o[2];
}

void Ray::setOriginPersp(float* o) {
    origin_persp[0] = o[0]; origin_persp[1] = o[1]; origin_persp[2] = o[2];
}

// this function is only intended for orthogrpahic, notice it uses -dir
void Ray::setDirection(float* dir) {
    direction[0] = -dir[0]; direction[1] = -dir[1]; direction[2] = -dir[2];
}

// calculates the ray direction for perspective viewing
void Ray::calculateDirectionPersp(float uR, float vR, float d, float* u, float* v, float* w) {
    direction_persp[0] = -d*w[0] + uR*u[0] + vR*v[0];
    direction_persp[1] = -d*w[1] + uR*u[1] + vR*v[1];
    direction_persp[2] = -d*w[2] + uR*u[2] + vR*v[2];
}

// calculates the ray origin for orthographic viewing
void Ray::calculateOriginOrtho(float uR, float vR, float* u, float* v, float* e) {

    origin[0] = e[0] + uR*u[0] + vR*v[0];
    origin[1] = e[1] + uR*u[1] + vR*v[1];
    origin[2] = e[2] + uR*u[2] + vR*v[2];
}

// ray sphere intersection using the orthographic attributes of the ray
float Ray::intersectSphere(float* center, float radius) {
    float omc[3] = {origin[0] - center[0],
                    origin[1] - center[1],
                    origin[2] - center[2]
                    };
    float A = direction[0]*direction[0] + 
              direction[1]*direction[1] + 
              direction[2]*direction[2];
    float B = (2.0f*direction[0]) * omc[0] +
              (2.0f*direction[1]) * omc[1] +
              (2.0f*direction[2]) * omc[2];
    float C = omc[0] * omc[0] +
              omc[1] * omc[1] +
              omc[2] * omc[2] -
              radius * radius ;
    float D = (B*B) - 4.0f*A*C;
    float q = 0;
    if(D<=0.0){return -1;}
 
    float t1 = ( -B + sqrt( (B*B)-4*A*C ) ) / (2*A);
    float t2 = ( -B - sqrt( (B*B)-4*A*C ) ) / (2*A);

    if (t1 >= 0 && t2 >= 0 && t2 < t1) {
        return t2;
    } else if (t2 >= 0 && t1 >= 0 && t1 < t2) {
        return t1;
    } else {
        return -1;
    }
}

// ray sphere intersection using the perspective attributes of the ray
float Ray::intersectSpherePersp(float* center, float radius) {
    float omc[3] = {origin_persp[0] - center[0],
                    origin_persp[1] - center[1],
                    origin_persp[2] - center[2]
                    };
    float A = direction_persp[0]*direction_persp[0] + 
              direction_persp[1]*direction_persp[1] + 
              direction_persp[2]*direction_persp[2];
    float B = (2.0f*direction_persp[0]) * omc[0] +
              (2.0f*direction_persp[1]) * omc[1] +
              (2.0f*direction_persp[2]) * omc[2];
    float C = omc[0] * omc[0] +
              omc[1] * omc[1] +
              omc[2] * omc[2] -
              radius * radius ;
    float D = (B*B) - 4.0f*A*C;
    float q = 0;
    if(D<=0.0){return -1;}
 
    float t1 = ( -B + sqrt( (B*B)-4*A*C ) ) / (2*A);
    float t2 = ( -B - sqrt( (B*B)-4*A*C ) ) / (2*A);

    if (t1 >= 0 && t2 >= 0 && t2 < t1) {
        return t2;
    } else if (t2 >= 0 && t1 >= 0 && t1 < t2) {
        return t1;
    } else {
        return -10;
    }
}
