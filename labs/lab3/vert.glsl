#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;
uniform mat3 transformation_matrix;

out vec3 myColor;

void main() {

    vec3 pos = transformation_matrix * vec3(aPos, 1.0);
    gl_Position = vec4(pos,  1.0);
    myColor = aColor;
}
