#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <cstdlib>
#include <ctime>

#define PI 3.141592654

const int SCREEN_WIDTH = 640.0;
const int SCREEN_HEIGHT = 640.0;
float camera_y = 0;
float camera_z = -20;
float modelView[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
float rotation_matrixX[16];
float rotation_matrixY[16];
float rotation_matrixZ[16];
float scale_matrix[16];
float trans_matrix[16];
float view_matrix[16];
float rotate_x = 0;
float rotate_y = 0;
float rotate_z = 0;
float trans_x = 0;
float trans_y = 0;
float trans_z = 0;
float angleX = 0;
float angleY = 0;
float angleZ = 0;
bool scale_x = false;
bool scale_y = false;
bool scale_z = false;
int oldXState;
int oldYState;
int oldZState;
float grow_x = 0;
float grow_y = 0;
float grow_z = 0;
float scale = 5.0;
float rotation_increment = .05;
float translation_increment = .2;
bool press_flag = false;
bool projection_flag = false;
bool space_flag = false;
bool shape_flag = false;
bool sFlag = false;
bool tFlag = false;
bool rFlag = false;
bool shape = false;

// OPENGL primitive Variables
GLuint VBO;

// describe vertex layout
GLuint VAO;

void updateModelView(float*, float*);
void scaleXYZ(float,float,float,float*);
void translateXYZ(float,float,float,float*);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    
    // shape switching
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
        space_flag = true;
    }
    else if (press_flag && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE) {
        shape_flag = !shape_flag;
        space_flag = false;
    }
    
    
    // camera movement keys
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        if (camera_y < 20) {camera_y += 0.5;}
    }
    else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        if (camera_y > -20) {camera_y -= 0.5;}
    }

    // scaling keys
    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        if (scale > 0.1) {scale -= 0.1;}
    }
    else if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        if (scale < 15) {scale += 0.1;}
    }
    
    // translation keys
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        trans_y -= translation_increment;
    } 
    else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        trans_y += translation_increment;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        trans_x += translation_increment;
    }
    else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        trans_x -= translation_increment;
    }
    if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS) {
        trans_z -= translation_increment;
    }
    else if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS) {
        trans_z += translation_increment;
    }

    // rotation keys
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
        rotate_x = -rotation_increment;
    }
    else if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
        rotate_x = rotation_increment;
    }
    else {
        rotate_x = 0;
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
        rotate_y = -rotation_increment;
    }
    else if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
        rotate_y = rotation_increment;
    }
    else {
        rotate_y = 0;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS) {
       rotate_z = -rotation_increment;
    }
    else if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS) {
        rotate_z = rotation_increment;
    }
    else {
        rotate_z = 0;
    }

    // projection switching key 
    if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_PRESS) {
        press_flag = true;
    }
    else if (press_flag && glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_RELEASE) {
        projection_flag = !projection_flag;
        press_flag = false;
    } 
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

void loadIdentity4x4(float* buffer) {
    for(int i = 0; i < 16; i++) {
        buffer[i] = 0.0f;
    }
    buffer[0] = 1.0f;
    buffer[5] = 1.0f;
    buffer[10] = 1.0f;
    buffer[15] = 1.0f;
}

void updateModelView(float* transf, float* buffer) {
    float temp[16];
    int tempCount = 0;
    for(int r = 0; r < 4; r++) {
        for(int c = 0; c < 4; c++) {
            temp[tempCount] = buffer[r*4] * transf[c] +
                    buffer[r*4+1] * transf[c+4] + 
                    buffer[r*4+2] * transf[c+8] + 
                    buffer[r*4+3] * transf[c+12];
            tempCount++;
        }
    }
    //
    for(int i = 0; i < sizeof(temp)/sizeof(temp[0]); i++){
        buffer[i] = temp[i];
        //
    }
}

void multMatrix4x4(float* b2, float* b1) {
    float temp[16];
    for(int r = 0; r < 4; r++) {
        for(int c = 0; c < 4; c++) {
            temp[r*4+c] = b1[r*4]*b2[c] + b1[r*4+1]*b2[c+4] + b1[r*4+2]*b2[c+8] + b1[r*4+3]*b2[c+12];
        }
    }
    for(int i = 0; i < 16; i++) {
        b2[i] = temp[i];
    }
}

void translateXYZ(float x, float y, float z, float* buffer) {
    buffer[0] = 1.0f;
    buffer[1] = 0.0f;
    buffer[2] = 0.0f;
    buffer[3] = x + 0.0f;
    buffer[4] = 0.0f;
    buffer[5] = 1.0f;
    buffer[6] = 0.0f;
    buffer[7] = y + 0.0f;
    buffer[8] = 0.0f;
    buffer[9] = 0.0f;
    buffer[10] = 1.0f;
    buffer[11] = z + 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void scaleXYZ(float x, float y, float z, float* buffer) {
    buffer[0] = x;
    buffer[1] = 0.0f;
    buffer[2] = 0.0f;
    buffer[3] = 0.0f;
    buffer[4] = 0.0f;
    buffer[5] = y;
    buffer[6] = 0.0f;
    buffer[7] = 0.0f;
    buffer[8] = 0.0f;
    buffer[9] = 0.0f;
    buffer[10] = z;
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void rotateX(float angle, float* buffer) {
    buffer[0] = 1.0f;
    buffer[1] = 0.0f;
    buffer[2] = 0.0f;
    buffer[3] = 0.0f;
    buffer[4] = 0.0f;
    buffer[5] = (float) cos(angle);
    buffer[6] = (float) -sin(angle);
    buffer[7] = 0.0f;
    buffer[8] = 0.0f;
    buffer[9] = (float) sin(angle);
    buffer[10] = (float) cos(angle);
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void rotateY(float angle, float* buffer) {
    buffer[0] = (float) cos(angle);
    buffer[1] = 0.0f;
    buffer[2] = (float) sin(angle);
    buffer[3] = 0.0f;
    buffer[4] = 0.0f;
    buffer[5] = 1.0f;
    buffer[6] = 0.0f;
    buffer[7] = 0.0f;
    buffer[8] = (float) -sin(angle);
    buffer[9] = 0.0f;
    buffer[10] = (float) cos(angle);
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void rotateZ(float angle, float* buffer) {
    buffer[0] = (float) cos(angle);
    buffer[1] = (float) -sin(angle);
    buffer[2] = 0.0f;
    buffer[3] = 0.0f;
    buffer[4] = (float) sin(angle);
    buffer[5] = (float) cos(angle);
    buffer[6] = 0.0f;
    buffer[7] = 0.0f;
    buffer[8] = 0.0f;
    buffer[9] = 0.0f;
    buffer[10] = 1.0f;
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void createScreenMatrix(float nx, float ny, float* buffer) {
    float temp = nx;
    nx = nx/ny;
    ny = ny/temp;
    buffer[0] = nx/2.0f;
    buffer[1] = 0.0f;
    buffer[2] = 0.0f;
    buffer[3] = (nx-1.0f)/2.0f;
    buffer[4] = 0.0f;
    buffer[5] = ny/2.0f;
    buffer[6] = 0.0f;
    buffer[7] = (ny-1.0f)/2.0f;
    buffer[8] = 0.0f;
    buffer[9] = 0.0f;
    buffer[10] = 1.0f;
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void createProjectionMatrix(float* buffer) {
    /* Method is used to create a float[16] array, containing the values needed to create the
     * projection matrix. Projection type can be switched between through the projection_flag
     * boolean. The functions recieves a pointer to an array to load the values into. */

    if (!projection_flag) {
        // perspective projection. 
        float imageAspectRatio = (SCREEN_WIDTH / SCREEN_HEIGHT);
        float n = 0.01;
        float f = 100;
        float max = 2;
        float r = max * imageAspectRatio, t = max; 
        float l = -r, b = -t; 

        buffer[0] = 2.0f / (r-l);
        buffer[1] = 0.0f;
        buffer[2] = 0.0f;
        buffer[3] = -((r+l)/(r-l));
        buffer[4] = 0.0f;
        buffer[5] = 2.0f / (t-b);
        buffer[6] = 0.0f;
        buffer[7] = -((t+b)/(t-b));
        buffer[8] = 0.0f;
        buffer[9] = 0.0f;
        buffer[10] = -2.0f / (-n+f);
        buffer[11] = -((n+f)/(-n+f));
        buffer[12] = 0.0f;
        buffer[13] = 0.0f;
        buffer[14] = 0.0f;
        buffer[15] = 1.0f;
        
        //std::cout << "PERSP" << std::endl;
    }
    else {
        // orthographic projection. Appears as an oblique.
        float n = 1;
        float f = -1;
        float r = 1, l = -r;
        float t = 1, b = -t;

        buffer[0] = (2*n) / (r-l);
        buffer[1] = 0.0f;
        buffer[2] = 0.0f;
        buffer[3] = 0.0f;
        buffer[4] = 0.0f;
        buffer[5] = (2*n) / (t-b);
        buffer[6] = 0.0f;
        buffer[7] = 0.0f;
        buffer[8] = (r+l) / (r-l);
        buffer[9] = (t+b)/(t-b);
        buffer[10] = -(f+n)/(f-n);
        buffer[11] = -1;
        buffer[12] = 0.0f;
        buffer[13] = 0.0f;
        buffer[14] = (-2*f*n)/(f-n);
        buffer[15] = 0.0f;

        //std::cout << "ORTHO" << std::endl;
    }
}

void createViewMatrix(float* buffer) {
    //our camera attributes: position, gaze, and the up direction
    float e[3] = {0, camera_y, camera_z};
    float g[3] = {0, 0, 0};
    float t[3] = {0, 1, 0};
        
    // create and normalize w
    float w[3] = {e[0] - g[0], e[1] - g[1], e[2] - g[2]};
    float mag = sqrt (pow(w[0], 2) + pow(w[1], 2) + pow(w[2], 2));
    w[0] = w[0] / mag;
    w[1] = w[1] / mag;
    w[2] = w[2] / mag;
        
    //create and normalize u
    float u[3] = {t[1]*w[2]-t[2]*w[1], t[2]*w[0]-t[0]*w[2], t[0]*w[1]-t[1]*w[0]};
    mag = sqrt (pow(u[0], 2) + pow(u[1], 2) + pow(u[2], 2));
    u[0] = u[0] / mag;
    u[1] = u[1] / mag;
    u[2] = u[2] / mag;
        
    // create v
    float v[3] = {w[1]*u[2]-w[2]*u[1], w[2]*u[0]-w[0]*u[2], w[0]*u[1]-w[1]*u[0]};
        
    // create the final view matrix
    buffer[0] = u[0];
    buffer[1] = v[0];
    buffer[2] = w[0];
    buffer[3] = 0;
    buffer[4] = u[1];
    buffer[5] = v[1];
    buffer[6] = w[1];
    buffer[7] = 0;
    buffer[8] = u[2];
    buffer[9] = v[2];
    buffer[10] = w[2];
    buffer[11] = 0;
    buffer[12] = -1 * (e[0]*u[0]) - (e[1]*u[1]) - (e[2]*u[2]);
    buffer[13] = -1 * (e[0]*v[0]) - (e[1]*v[1]) - (e[2]*v[2]);
    buffer[14] = -1 * (e[0]*w[0]) - (e[1]*w[1]) - (e[2]*w[2]);
    buffer[15] = 1;
}

void sphere(int n, float* wedges){
    float increment = 2.0f*PI/(float)n;         // full revolution of unit circle is 2PI
    int count = 0;
    float circle[n][4];
    for(int i = 0; i < n; i++){ 
        circle[i][0] = cos(i*increment);
        circle[i][1] = sin(i*increment);
        circle[i][2] = 0.0f;
        circle[i][3] = 0.0f;
    }
    for(int circ_c = 0; circ_c < n/2; circ_c++){
        float angle1 = (float)(circ_c/(float)n*360);
        angle1 = angle1*PI/180.0f;
        float angle2 = (float)((circ_c+1)/(float)n*360);
        angle2 = angle2*PI/180.0f;
        float angleStorage[16];
        for(int i = 0; i < n; i++) {
            float r = rand() % 100 / 100.0;
            float g = rand() % 100 / 100.0;
            float b = rand() % 100 / 100.0;
            float xyz1[16] = {0.0f,0.0f,0.0f,circle[i][0],0.0f,0.0f,0.0f,circle[i][1],0.0f,0.0f,0.0f,circle[i][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle1, angleStorage); multMatrix4x4(xyz1, angleStorage); std::fill_n(angleStorage,16,0);
            float xyz2[16] = {0.0f,0.0f,0.0f,circle[(i+1)%n][0],0.0f,0.0f,0.0f,circle[(i+1)%n][1],0.0f,0.0f,0.0f,circle[(i+1)%n][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle1, angleStorage); multMatrix4x4(xyz2, angleStorage); std::fill_n(angleStorage,16,0);
            float xyz3[16] = {0.0f,0.0f,0.0f,circle[i][0],0.0f,0.0f,0.0f,circle[i][1],0.0f,0.0f,0.0f,circle[i][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle2, angleStorage); multMatrix4x4(xyz3, angleStorage); std::fill_n(angleStorage,16,0);
            float xyz4[16] = {0.0f,0.0f,0.0f,circle[(i+1)%n][0],0.0f,0.0f,0.0f,circle[(i+1)%n][1],0.0f,0.0f,0.0f,circle[(i+1)%n][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle2, angleStorage); multMatrix4x4(xyz4, angleStorage); std::fill_n(angleStorage,16,0);
            wedges[count + 0] = xyz1[3]; wedges[count + 1] = xyz1[7]; wedges[count + 2] = xyz1[11]; wedges[count + 3] = r; wedges[count + 4] = g; wedges[count + 5] = b;
            count+=6;
            wedges[count + 0] = xyz2[3]; wedges[count + 1] = xyz2[7]; wedges[count + 2] = xyz2[11]; wedges[count + 3] = r; wedges[count + 4] = g; wedges[count + 5] = b;
            count+=6;
            wedges[count + 0] = xyz3[3]; wedges[count + 1] = xyz3[7]; wedges[count + 2] = xyz3[11]; wedges[count + 3] = r; wedges[count + 4] = g; wedges[count + 5] = b;
            count+=6;
            wedges[count + 0] = xyz2[3]; wedges[count + 1] = xyz2[7]; wedges[count + 2] = xyz2[11]; wedges[count + 3] = r; wedges[count + 4] = g; wedges[count + 5] = b;
            count+=6;
            wedges[count + 0] = xyz3[3]; wedges[count + 1] = xyz3[7]; wedges[count + 2] = xyz3[11]; wedges[count + 3] = r; wedges[count + 4] = g; wedges[count + 5] = b;
            count+=6;
            wedges[count + 0] = xyz4[3]; wedges[count + 1] = xyz4[7]; wedges[count + 2] = xyz4[11]; wedges[count + 3] = r; wedges[count + 4] = g; wedges[count + 5] = b;
            count+=6;
        }
    }
}

int main(int argc, char** argv) {
    srand(time(NULL));
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    int verts = atoi(argv[1]);
    int SPHERE_BUFFER_SZ = 18*(verts*verts);
    float vertices[SPHERE_BUFFER_SZ];
    sphere(verts,vertices);

    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();
    // and use z-buffering
    glEnable(GL_DEPTH_TEST);
    
    float maxX = 1.0f;
    float maxY = 1.0f;
    float r = 1.0f;
    float l = -r;
    float t = 1;
    float b = -t;
    float n = 1.0f;
    float f = -1.0f;
    int loc = 0;
    float projection[16];
    float screen[16];
    float view[16];

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);
        
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        angleX = (angleX + rotate_x);
        angleY = (angleY + rotate_y);
        angleZ = (angleZ + rotate_z);
        
        scaleXYZ(scale, scale, scale, scale_matrix);
        rotateX(angleX, rotation_matrixX);
        rotateY(angleY, rotation_matrixY);
        rotateZ(angleZ, rotation_matrixZ);
        translateXYZ(trans_x, trans_y, trans_z, trans_matrix);

        loc  = glGetUniformLocation(shader.id(), "rotationX");
        glUniformMatrix4fv(loc, 1, GL_FALSE, rotation_matrixX);
        loc  = glGetUniformLocation(shader.id(), "rotationY");
        glUniformMatrix4fv(loc, 1, GL_FALSE, rotation_matrixY);
        loc  = glGetUniformLocation(shader.id(), "rotationZ");
        glUniformMatrix4fv(loc, 1, GL_FALSE, rotation_matrixZ);
        
        loc  = glGetUniformLocation(shader.id(), "scale");
        glUniformMatrix4fv(loc, 1, GL_FALSE, scale_matrix);
        
        loc  = glGetUniformLocation(shader.id(), "trans");
        glUniformMatrix4fv(loc, 1, GL_FALSE, trans_matrix);
                
        createProjectionMatrix(projection);
        loc  = glGetUniformLocation(shader.id(), "proj");
        glUniformMatrix4fv(loc, 1, GL_FALSE, projection);
                
        createScreenMatrix(SCREEN_WIDTH,SCREEN_HEIGHT, screen);
        loc  = glGetUniformLocation(shader.id(), "screen");
        glUniformMatrix4fv(loc, 1, GL_FALSE, screen);
                
        createViewMatrix(view);
        loc  = glGetUniformLocation(shader.id(), "view");
        glUniformMatrix4fv(loc, 1, GL_FALSE, view);
        
        // activate shader
        shader.use();
        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));
        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
        
        loadIdentity4x4(modelView);
        updateModelView(rotation_matrixX, modelView);
        updateModelView(rotation_matrixY, modelView);
        updateModelView(rotation_matrixZ, modelView);
        updateModelView(scale_matrix, modelView);
        updateModelView(trans_matrix, modelView);
    }

    glfwTerminate();
    return 0;
}
