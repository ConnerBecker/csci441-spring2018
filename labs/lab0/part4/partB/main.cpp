#include <iostream>
#include <string>

int main(int argc, char** argv) {
	std::cout << "Please enter your name..." << std::endl;
	std::string name;
	std::cin >> name;
	std::cout << "Hello " << name << std::endl;
}
