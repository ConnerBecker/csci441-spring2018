#include <iostream>

class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 add(Vector3 v, Vector3 v2) { // v and v2 are copies, so any changes to them in this function
                                     // won't affect the originals
    // ...
    float newX = v.x + v2.x;
    float newY = v.y + v2.y;
    float newZ = v.z + v2.z;
    return Vector3(newX, newY, newZ);
}

int main(int argc, char** argv) {
    Vector3 a(1,2,3);   // allocated to the stack
    Vector3 b(4,5,6);

    Vector3 c = add(a,b); // copies 6 floats to the arguments in add (3 per Vector3),
                          // 3 more floats copied into c when it returns
                          // a and b are unchanged
    
    std::cout << "Vector C xyz: " << c.x << "," << c.y << "," << c.z << std::endl;
}

