#include <iostream>

class Vector3 {
public:
    float x;
    float y;
    float z;
    Vector3();
	
    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3::Vector3(){}

Vector3 operator+(Vector3 v, Vector3 v2) {
    float newX = v.x + v2.x;
    float newY = v.y + v2.y;
    float newZ = v.z + v2.z;
    return Vector3(newX, newY, newZ);
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    
    stream << v.x << "," << v.y << "," << v.z;
    return stream;
}

int main(int argc, char** argv) {

    int vectorCount = 10;
    Vector3 vectors[vectorCount];   // allocated to the stack
    Vector3* heapVectors = new Vector3[vectorCount]; // allocated to heap
    
    for(int i = 0; i < vectorCount; i++){
    	heapVectors[i].y = 5;
        std::cout << heapVectors[i] << std::endl;
    }
    
    delete [] heapVectors;
}

