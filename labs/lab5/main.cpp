/*
 * Conner Becker, Samuel Congdon, Chance Ronning
 * CSCI 441: Graphics
 * Lab 5: Materials and Lights
 * March 6, 2018
 *
 * Controls:
 *  W,S: light source y movement
 *  A,D: light source x movement
 *  Z,X: light source z movement
 *  Note: light source initialized at (0,0,0)
 *
 *  /: switch between displayed shapes (cylinder and sphere)
 *
 *  Shape movement is implemented as was in the supplied code.
 *
 * This c++ program demonstrates the application of Phong shading through implementing three
 * techniques: diffuse, specular, and ambient shading. To achieve this, generated shapes also had
 * their associated normals calculated upon initialization, which are used by the vertex and
 * fragment shaders to achieve the lighting effects. The reduced, transposed, and inverted model
 * matrix is also created within the main.cpp file, being later passed into the shaders. This allows
 * the matrix to be calculated on the CPU instead of the GPU. Both the shape and light source can be
 * moved wihtin the viewspace through user input. The programs light source is statically set to be
 * white (1,1,1). The amplitude of the specular and ambient effects are also statically defined
 * within the fragment shader (frag.glsl). The current settings for the specular lighting are rather
 * intesnse, this is to clearly display its behaviour.
 *
 */

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <vector>
#include <map>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
float light_x = 0;
float light_y = 0;
float light_z = 0;
float light_step = 0.05;
bool shape_flag = false;
bool press_flag = false;
bool new_shape = true;
float camera_position[3] = {0, 0, -2};
//std::unordered_map<std::vector<float>, std::vector<float>> triangles;
std::map<std::vector<float>, std::vector<float>> triangles;

void calculate_smooth_normal(std::vector<float>& v) {
    // store all the face normals for each vertex
    // std::cout << "coords size : " << v.size() << std::endl;
    for (int i=0;i<v.size();i+=9) {
        // std::cout << "checking coord " << std::endl;
        std::vector<float> vertex = {v[i], v[i+1], v[i+2]};
        if (triangles.count(vertex) > 0) {std::cout << "Hit" << i << std::endl;}
        else {
            triangles[vertex] = {v[i+6], v[i+7], v[i+8]};
            for (int j=i+9;j<v.size();j+=9) {
                std::vector<float> check_vertex = {v[j], v[j+1], v[j+2]};
                if (vertex == check_vertex) {
                    triangles[vertex].push_back(v[j+6]);
                    triangles[vertex].push_back(v[j+7]);
                    triangles[vertex].push_back(v[j+8]);
                }
            }
        }
    }// end find normals for

    std::cout << "size of map: " << triangles.size() << std::endl;
    // std::cout << "triangles.begin: " << triangles.begin() << "triangles.end: " << triangles.end() << std::endl;

    for (std::map<std::vector<float>, std::vector<float>>::iterator it = triangles.begin();it != triangles.end(); it++) {
        std::vector<float> normals = it->second;

        // float first,second,third;
        float first = 0;
        float second = 0;
        float third = 0;
        for (int i=0;i<normals.size();i+=3) {
            // std::cout << "updated normals" << normals[i] << ", " << normals[i+1] << ", " << normals[i+2] << ", " << std::endl;
            first += normals[i];
            second += normals[i+1];
            third += normals[i+2];
        }
        first = first / (normals.size() / 3);
        second = second / (normals.size() / 3);
        third = third / (normals.size() / 3);
                std::cout << "smooth normals" << first << ", " << second << ", " << third << ", " << std::endl;

        // triangles[it->first].clear();
        // triangles[it->first].push_back(first);
        // triangles[it->first].push_back(second);
        // triangles[it->first].push_back(third);

        for (int i=0;i<v.size();i+=9) {
            std::vector<float> check_vertex = {v[i], v[i+1], v[i+2]};
            if (check_vertex == it->first) {
                // std::cout << "changing normals" << std::endl;
                // std::cout << "original normals" << v[i+6] << ", " << v[i+7] << ", " << v[i+8] << ", " << std::endl;
                // std::cout << "smooth normals" << first << ", " << second << ", " << third << ", " << std::endl;

                v[i+6] = first;
                v[i+7] = second;
                v[i+8] = third;

                // std::cout << "updated normals" << v[i+6] << ", " << v[i+7] << ", " << v[i+8] << ", " << std::endl;
            }
        }
    }


}// end smooth_normals

void calculateNormalVector(std::vector<float> v1, std::vector<float> v2, std::vector<float> v3, float* buf) {
    /* Calculates the normal vector for each of the fragments contained in the shape vector. The
     * two possible normals are calculated, then the normal with the larger distance from the center
     * of the shape. This allows accurate normals to be calculated for any solid shapes centered
     * about the origin, without hollow space in the center. Thus, this works for the sphere,
     * cylinder, and cube. */

    float b[3] = {v1[0]-v2[0], v1[1] - v2[1], v1[2] - v2[2]};
    float a[3] = {v1[0]-v3[0], v1[1] - v3[1], v1[2] - v3[2]};
    float a2[3] = {v2[0]-v1[0], v2[1] - v1[1], v2[2] - v1[2]};
    float b2[3] = {v3[0]-v1[0], v3[1] - v1[1], v3[2] - v1[2]};

    float cross1[3] = {a[1]*b[2] - a[2]*b[1],
                       a[2]*b[0] - a[0]*b[2],
                       a[0]*b[1] - a[1]*b[0]};
    float cross2[3] = {a2[1]*b2[2] - a2[2]*b2[1],
                       a2[2]*b2[0] - a2[0]*b2[2],
                       a2[0]*b2[1] - a2[1]*b2[0]};

    float distance1 = std::sqrt(std::pow(cross1[0]+v1[0],2) + std::pow(cross1[1]+v1[1],2) + std::pow(cross1[2]+v1[2],2));
    float distance2 = std::sqrt(std::pow(cross2[0]+v1[0],2) + std::pow(cross2[1]+v1[1],2) + std::pow(cross2[2]+v1[2],2));

    std::cout << distance1 << " Buf1: " << cross1[0] << cross1[1] << cross1[2] << std::endl;
    std::cout << distance2 << " Buf2: " << cross2[0] << cross2[1] << cross2[2] << std::endl;
    if(distance1 > distance2) {
        buf[0] = cross1[0];
        buf[1] = cross1[1];
        buf[2] = cross1[2];
    }
    else {
        buf[0] = cross2[0];
        buf[1] = cross2[1];
        buf[2] = cross2[2];
    }

}

void resizeVectorArray(std::vector<float>& v) {
    /* Resizes the vector array in order to place the normal vectors relative to their fragments */
    int startingSize = v.size();
    std::vector<float> newV;
    for(int i = 1; i <= startingSize; i++) {
        newV.push_back(v[i-1]);
        if(i != 1 && i%6 == 0) {
            newV.push_back(0.0);
            newV.push_back(0.0);
            newV.push_back(0.0);
        }
    }
    v = newV;
}

void insertNormalVectors(std::vector<float>& v) {
    /* Inserts the calculated normal vectors into their related positions in the shapes vector */
    std::vector<float>::iterator itBegin;
    std::vector<float>::iterator itEnd;
    for(int i = 0; i < v.size(); i+=27) {
        itBegin = v.begin()+ i; itEnd = v.begin() + i + 9;
        std::vector<float> v1(itBegin, itEnd);
        itBegin = v.begin() + i + 9; itEnd = v.begin() + i + 18;
        std::vector<float> v2(itBegin, itEnd);
        itBegin = v.begin() + i + 18; itEnd = v.begin() + i + 27;
        std::vector<float> v3(itBegin, itEnd);

        float norm[3];
        calculateNormalVector(v1, v2, v3, norm);
        v[i+6] = norm[0];
        v[i+7] = norm[1];
        v[i+8] = norm[2];
        v[i+15] = norm[0];
        v[i+16] = norm[1];
        v[i+17] = norm[2];
        v[i+24] = norm[0];
        v[i+25] = norm[1];
        v[i+26] = norm[2];
    }
}

bool translate_invert_matrix(const float* m, float* invOut) {
    /* Function to calculate the transposed inverse of a 3x3 matrix from the passed in 4x4 model
     * matrix. Only the top left 3x3 area is used in order to ignore the effects of translation on
     * the normal matrix. Returns false if the determinant is found to be zero, otherwise returns
     * true. */
    float inv[9], det;
    int i;

    inv[0] = m[5]*m[10] - m[9]*m[6];
    inv[3] = -(m[1]*m[10] - m[2]*m[9]);
    inv[6] =  m[1]*m[6] - m[2]*m[5];
    inv[1] = -(m[4]*m[10] - m[6]*m[8]);
    inv[4] = m[0]*m[10] - m[2]*m[8];
    inv[7] = -(m[0]*m[6] - m[4]*m[2]);
    inv[2] =  m[4]*m[9] - m[8]*m[5];
    inv[5] = -(m[0]*m[9] - m[8]*m[1]);
    inv[8] =  m[0]*m[5] - m[4]*m[1];

    det = m[0]*m[5]*m[10] + m[4]*m[9]*m[2] + m[8]*m[1]*m[6] - m[8]*m[5]*m[2] - m[4]*m[1]*m[10] - m[0]*m[9]*m[6];

    if (det == 0)
        return false;

    det = 1/det;

    for (i = 0; i < 9; i++)
        invOut[i] = inv[i] * det;

    return true;
}


void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    // controls to move the light source and change the shape shown
    else if (isPressed(window, GLFW_KEY_W)) {light_y += light_step;}
    else if (isPressed(window, GLFW_KEY_S)) {light_y -= light_step;}
    else if (isPressed(window, GLFW_KEY_A)) {light_x += light_step;}
    else if (isPressed(window, GLFW_KEY_D)) {light_x -= light_step;}
    else if (isPressed(window, GLFW_KEY_Z)) {light_z += light_step;}
    else if (isPressed(window, GLFW_KEY_X)) {light_z -= light_step;}
    else if (isPressed(window, GLFW_KEY_SLASH)) {press_flag = true;}
    else if (press_flag && isReleased(window, GLFW_KEY_SLASH)) {
        new_shape = true;
        shape_flag = !shape_flag;
        press_flag = false;}

    model = processModel(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "lab5", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create c
    Cylinder c(20, 1, .2, .4);
    // Cone c(20, 1, .2, .4);
    // Sphere c(20, .5, 1, .2, .4);
    //Torus c(20, .75, .25, 1, .2, .4);
    //DiscoCubeN c;

    // create the normals
    resizeVectorArray(c.coords);
    insertNormalVectors(c.coords);

    // copy vertex data
    GLuint VBO1;
    glGenBuffers(1, &VBO1);
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, c.coords.size()*sizeof(float),
            &c.coords[0], GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO1;
    glGenVertexArrays(1, &VAO1);
    glBindVertexArray(VAO1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);

    // create t
    Cylinder t(20, 1, .2, .4);
    // Cone t(20, 1, .2, .4);
    // Sphere t(20, .5, 1, .2, .4);
    //Torus t(20, .75, .25, 1, .2, .4);
    //DiscoCube t;

    // create the normals
    resizeVectorArray(t.coords);
    insertNormalVectors(t.coords);
    std::cout << "Original" << t.coords[6] << std::endl;
    calculate_smooth_normal(t.coords);
    std::cout << "Smooth" << t.coords[6] << std::endl;

    // copy vertex data
    GLuint VBO2;
    glGenBuffers(1, &VBO2);
    glBindBuffer(GL_ARRAY_BUFFER, VBO2);
    glBufferData(GL_ARRAY_BUFFER, t.coords.size()*sizeof(float),
            &t.coords[0], GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO2;
    glGenVertexArrays(1, &VAO2);
    glBindVertexArray(VAO2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);


    // setup projection
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    // setup view
    Vector eye(camera_position[0], camera_position[1], camera_position[2]);
    Vector origin(0, 0, 0);
    Vector up(0, 1, 0);

    Matrix camera;
    camera.look_at(eye, origin, up);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // set the matrices
    Matrix model;

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    float ITModel[9];

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(model, window);

        // create the transposed inverse of the model matrix
        bool ret = translate_invert_matrix(model.values, ITModel);
        if(!ret){std::cout << "#### Det is 0!" << std::endl;}

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        // create and pass the light position to the shader
        float lightPos[3] = {light_x, light_y, light_z};
        int loc  = glGetUniformLocation(shader.id(), "light");
        glUniform3fv(loc, 1, lightPos);

        // pass the camera position to the shader
        loc  = glGetUniformLocation(shader.id(), "cameraPosition");
        glUniform3fv(loc, 1, camera_position);

        // pass the inversed transpose matrix of the model to the shader
        loc  = glGetUniformLocation(shader.id(), "ITModel");
        glUniformMatrix3fv(loc, 1, GL_FALSE, ITModel);

        Uniform::set(shader.id(), "model", model);
        Uniform::set(shader.id(), "projection", projection);
        Uniform::set(shader.id(), "camera", camera);

        // render the current;y selected shape
        if (shape_flag) {
            glBindVertexArray(VAO1);
            glDrawArrays(GL_TRIANGLES, 0, c.coords.size()*sizeof(float));
        } else {
            glBindVertexArray(VAO2);
            glDrawArrays(GL_TRIANGLES, 0, t.coords.size()*sizeof(float));
        }

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
