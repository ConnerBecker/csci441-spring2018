/*
   Conner Becker
   CSCI 441: Graphics
   Program1: 2D Basic Paint
   March 7, 2018
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <cmath>
#include <string>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Node.cpp"
#include "Primitives.cpp"
#include "ObjectLoader.cpp"

#define VERTEX_SIZE 5
#define WINDOW_W 640
#define WINDOW_H 640
#define LOG_BUFFER_SIZE 512
#define PI 3.141592654

ShapeNode* player;
ShapeNode* maze;
ShapeNodeList list;

int slashState;

float player_x_angle = 0;
float player_y_angle = 0;
float player_z_angle = 0;

float move_player_x = 0.0f;
float move_player_y = 0.25f;
float move_player_z = 1.0f;

float player_trans[3] = {move_player_x, move_player_y, move_player_z};
float player_rot[3] = {0, 0, 0};

float origin[3] = {move_player_x, move_player_y, move_player_z};
float up[3] = {0.0f, 1.0f, 0.0f};
float camera_pos[3] = {move_player_x, move_player_y, move_player_z};

float light_pos[3] = {0.0f,3.0f,1.0f};

GLuint vertexShader;
GLuint fragmentShader;
GLuint shaderProgram;

float glXCoords(float x, float width){
    return -1.0f + 2.0f*(x/width);
}

float glYCoords(float y, float height){
    return -1.0f + (2.0f - 2.0f*(y/height));
}

void cursor_pos_callback(GLFWwindow* window, int button, int action, int mod) {
    double x;
    double y;
    
    glfwGetCursorPos(window, &x, &y);
    
    x = glXCoords(x, WINDOW_W);
    y = glYCoords(y, WINDOW_H);
    
}

void bindStaticArray(float* buf) {
    glBufferData(GL_ARRAY_BUFFER, sizeof(buf), buf, GL_STATIC_DRAW);
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    
    if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_RELEASE) {
        if (slashState == GLFW_PRESS) {
            list.changeView();
            slashState = GLFW_RELEASE;
            if (list.viewMode) {
                list.changePerspective(60, 1, .01, 10);
                up[2] = 0.0f;
                up[1] = 1.0f;
            } else {
                list.changePerspective(-1, 1, -1, 1, -5, 1); // -10 , .1
                up[2] = -1.0f;
                up[1] = 0.0f;
            }
        }
    } else {
        slashState = GLFW_PRESS; 
    }
    
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        player_y_angle += 1;
    } else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        player_y_angle -= 1;
    }
    
    float xPortion = .01 * fabs(sin(player_y_angle*PI/180.0));
    float zPortion = .01 * fabs(cos(player_y_angle*PI/180.0));
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        if (sin(player_y_angle*PI/180.0) < 0) {
            move_player_x += xPortion;
        } else {
            move_player_x += -xPortion;
        }
        if (cos(player_y_angle*PI/180.0) < 0) {
            move_player_z += zPortion;    
        } else {
            move_player_z += -zPortion;
        }
    } else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        if (sin(player_y_angle*PI/180.0) < 0) {
            move_player_x += -xPortion;
        } else {
            move_player_x += xPortion;
        }
        if (cos(player_y_angle*PI/180.0) < 0) {
            move_player_z += -zPortion;    
        } else {
            move_player_z += zPortion;
        }
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(WINDOW_W, WINDOW_H, "Prog 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }
    
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    GLuint shader = 0;
    
    shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, NULL);
    glCompileShader(shader);
    
    int success;
    char log[LOG_BUFFER_SIZE];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, LOG_BUFFER_SIZE, NULL, log);
        std::cerr << "#ERROR SHADER#" << shaderTypeName(shaderType)
            <<"-COMPILATION_FAILED\n"
            << log << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {

    GLuint program = 0;
    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char log[LOG_BUFFER_SIZE];
        glGetProgramInfoLog(program, LOG_BUFFER_SIZE, NULL, log);
        std::cerr << "#ERROR PROGRAM#-COMPILATION_FAILED\n"
            << log << std::endl;
    }

    return program;
}

int main(void) {
    srand(time(0));
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }

    vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

    
    shaderProgram = createShaderProgram(vertexShader, fragmentShader);


    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    
    glfwSetMouseButtonCallback(window, cursor_pos_callback);
    
    glUseProgram(shaderProgram);
    Loader l;
    // load in the car obj file
    std::vector<float> dino = l.loadOBJFile("../models/car.obj");
    // load in the maze using my defined format # = wall 0 = open space
    std::vector<float> m = l.loadMaze("../models/myMaze.conner");
    
    // adding shapes to linkedl list                          
    list.addNewShape(dino);
    list.addNewShape(m);
    
    // adding light and eye for viewing
    list.addLight(light_pos);
    list.addEye(camera_pos);
    player = list.first;
    maze = player->next;
    list.changePerspective(60, 1, .01, 10);
    
    list.registerShapes(shaderProgram);
    
    std::cout << "Entering Loop..." << std::endl;

    glEnable(GL_DEPTH_TEST);
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_DEPTH_BUFFER_BIT);
        processInput(window);

        glClearColor(.2f,.3f,.4f,1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        if(list.isNotEmpty()){
        
            // setting up rotation array to pass to player move
            player_rot[0] = player_x_angle;
            player_rot[1] = player_y_angle;
            player_rot[2] = player_z_angle;
            // setting up translation array to get camera position
            player_trans[0] = move_player_x;
            player_trans[1] = move_player_y;
            player_trans[2] = move_player_z;
            
            // move light above player
            list.moveLight(move_player_x, move_player_y+2, move_player_z);
            // move car model just under the camera
            player->moveToPlayer(player_trans, player_rot, .05);
            // face the camera in front of car
            list.face_camera(camera_pos, origin, up, 
                             player_x_angle, player_y_angle, player_trans);
            list.drawShapes();
        }
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    std::cout << "Leaving Render Loop..." << std::endl;

    glfwTerminate();
    return 0;
}





