#include <math.h>
#include <vector>
#define PI 3.141592654

class Circle {
    public:
    Circle();
    std::vector<float> coords;
    std::vector<float> makeCircle(float, int, float, float);
};

Circle::Circle() {
    std::cout << "Created Circle" << std::endl;
}

std::vector<float> Circle::makeCircle(float scale, int v, float x, float y){
    float increment = 2*PI/v;
    
    for(int i = 0; i < v; i++){
        coords.push_back(cos(increment*i)/scale + x);
        coords.push_back(sin(increment*i)/scale + y);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(cos(increment*(i+1))/scale + x);
        coords.push_back(sin(increment*(i+1))/scale + y);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(x);
        coords.push_back(y);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
    }
    
    return coords;
}

class Square {
    public:
    Square();
    std::vector<float> coords;
    std::vector<float> makeSquare(float, float, float, float);
};

Square::Square() {
    std::cout << "Created Square" << std::endl;
}

std::vector<float> Square::makeSquare(float w, float h, float x, float y){
        coords.push_back(x + w/2.0f);
        coords.push_back(y + h/2.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(x - w/2.0f);
        coords.push_back(y + h/2.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(x - w/2.0f);
        coords.push_back(y - h/2.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(x - w/2.0f);
        coords.push_back(y - h/2.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(x + w/2.0f);
        coords.push_back(y - h/2.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(x + w/2.0f);
        coords.push_back(y + h/2.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        return coords;
}

class Triangle {
    public:
    Triangle();
    std::vector<float> coords;
    std::vector<float> makeTriangle(float, float, float, float);
};

Triangle::Triangle() {
    std::cout << "Created Triangle" << std::endl;
}

std::vector<float> Triangle::makeTriangle(float w, float h, float x, float y){
        coords.push_back(w/2.0f + x);
        coords.push_back(0.0f + y);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(-w/2.0f + x);
        coords.push_back(0.0f+ y);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        coords.push_back(0.0f + x);
        coords.push_back(h + y);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        coords.push_back(rand()%10/10.0f);
        
        return coords;
}
        
