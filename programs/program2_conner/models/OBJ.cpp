#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <ctime>
#include <cmath>
#include <string.h>
#include <vector>
#include <map>

class Loader {
    public:
    Loader();
    std::vector<float> loadOBJFile(char*);
    float lastNorm[3] = {0,-1,-1};
    void smooth_normals(std::vector<float>*);
    void calc_normal(std::vector<float>,std::vector<float>,std::vector<float>,float*);
    float det(float, float, float, float);
};

Loader::Loader() {
    std::cout << "Created Loader" << std::endl;
}

void Loader::smooth_normals(std::vector<float>* v) {
    
    std::map<std::vector<float>, std::vector<float>> vectorMap;
    for (int i = 0; i < v->size(); i+=9) {
        std::vector<float> v_temp = {v->at(i), v->at(i+1), v->at(i+2)};
        if (vectorMap.count(v_temp) == 0) {
            vectorMap[v_temp] = {v->at(i+6), v->at(i+7), v->at(i+8)};
            for (int j=i+9;j<v->size();j+=9) {
                std::vector<float> check_vertex = {v->at(j), v->at(j+1), v->at(j+2)};
                if (v_temp == check_vertex) {
                    vectorMap[v_temp].push_back(v->at(j+6));
                    vectorMap[v_temp].push_back(v->at(j+7));
                    vectorMap[v_temp].push_back(v->at(j+8));
                }
            }
        }
    }
    
    for (std::map<std::vector<float>, std::vector<float>>::iterator it = vectorMap.begin();it != vectorMap.end(); it++) {
        std::vector<float> normals = it->second;

        float first = 0;
        float second = 0;
        float third = 0;
        for (int i=0;i<normals.size();i+=3) {

            first += normals[i];
            second += normals[i+1];
            third += normals[i+2];
        }
        first = first / (normals.size() / 3);
        second = second / (normals.size() / 3);
        third = third / (normals.size() / 3);
        
        for (int i=0;i<v->size();i+=9) {
            std::vector<float> check_vertex = {v->at(i), v->at(i+1), v->at(i+2)};
            if (check_vertex == it->first) {
                
                v->at(i+6) = first;
                v->at(i+7) = second;
                v->at(i+8) = third;
            }
        }
    }
}

float Loader::det(float a, float b, float c, float d) {
    return a*d - c*b;
}

void Loader::calc_normal(std::vector<float> v1, 
                         std::vector<float> v2, 
                         std::vector<float> v3, float* buf) {

    /* Calculates the normal vector for each of the fragments contained in the shape vector. The
     * two possible normals are calculated, then the normal with the larger distance from the center
     * of the shape. This allows accurate normals to be calculated for any solid shapes centered
     * about the origin, without hollow space in the center. Thus, this works for the sphere,
     * cylinder, and cube. */
    float t1[3] = {
                        det(v1[1], v1[2], v2[1], v2[2]),
                        -det(v1[0], v1[2], v2[0], v2[2]),
                        det(v1[0], v1[1], v2[0], v2[1])
                     };
    float t2[3] = {
                        det(v1[1], v1[2], v3[1], v3[2]),
                        -det(v1[0], v1[2], v3[0], v3[2]),
                        det(v1[0], v1[1], v3[0], v3[1])
                     };
    float d1 = sqrt(pow(t1[0]-lastNorm[0],2) + pow(t1[1]-lastNorm[1],2) + pow(t1[2]-lastNorm[2],2));
    float d2 = sqrt(pow(t2[0]-lastNorm[0],2) + pow(t2[1]-lastNorm[1],2) + pow(t2[2]-lastNorm[2],2));
    if (d1 < d2) {
        buf[0] =  t1[0];
        buf[1] =  t1[1];
        buf[2] =  t1[2];
        
        lastNorm[0] =  t1[0];
        lastNorm[1] =  t1[1];
        lastNorm[2] =  t1[2];
    } else {
        buf[0] =  t2[0];
        buf[1] =  t2[1];
        buf[2] =  t2[2];
        
        lastNorm[0] =  t2[0];
        lastNorm[1] =  t2[1];
        lastNorm[2] =  t2[2];
    }
    
    /*
    float dv1[3] = {
        v1[0] - v3[0],
        v1[1] - v3[1],
        v1[2] - v3[2]
    };
    float dv2[3] = {
        v2[0] - v3[0],
        v2[1] - v3[1],
        v2[2] - v3[2]
    };
    float t[3] = { det(dv1[1], dv1[2], dv2[1], dv2[2]),
                  -det(dv1[0], dv1[2], dv2[0], dv2[2]),
                   det(dv1[0], dv1[1], dv2[0], dv2[1])
    };
    
    buf[0] =  t[0];
    buf[1] =  t[1];
    buf[2] =  t[2];
    */
}

std::vector<float> Loader::loadOBJFile (char* fp) {
    std::vector<std::vector<float> > points;
    std::vector<float> opengl_obj;
    std::fstream objFile(fp);
    std::string line;
    float r = 0.1f;
    float g = 0.9f;
    float b = 0.1f;
    
    while (std::getline(objFile,line)) {
    
        if (line[0] == 'v') {
        
            char* tok;
            char* str = new char[line.length()+1];
            std::vector<float> vert;
            std::strcpy(str, line.c_str());
            tok = strtok(str, " ");
            
            vert.push_back(atof(strtok(NULL, " ")));
            vert.push_back(atof(strtok(NULL, " ")));
            vert.push_back(atof(strtok(NULL, " ")));
            points.push_back(vert);
        }
    }
    
    objFile.clear();
    objFile.seekg(0, std::ios::beg);

    while (std::getline(objFile,line)) {
    
        if (line[0] == 'f') {
        
            char* tok;
            char* str = new char[line.length()+1];
            float norm[3];
            float vert[3];
            std::vector<float> v1,v2,v3;
            std::strcpy(str, line.c_str());
            tok = strtok(str, " ");
            
            vert[0] = atoi(strtok(NULL, " "))-1;
            vert[1] = atoi(strtok(NULL, " "))-1;
            vert[2] = atoi(strtok(NULL, " "))-1;
            
            v1 = points[vert[0]];
            v2 = points[vert[1]];
            v3 = points[vert[2]];

            calc_normal(v1,v2,v3,norm);
            for (int i = 0; i < 3; i++) {
                float x,y,z;
                //r = (rand() % 50 + 50) / 100.0;
                //g = (rand() % 100) / 100.0;
                //b = (rand() % 100) / 100.0;
                r = r+=.001;
                
                x = points[vert[i]][0];
                y = points[vert[i]][1];
                z = points[vert[i]][2];
                opengl_obj.push_back(x);
                opengl_obj.push_back(y);
                opengl_obj.push_back(z);
                opengl_obj.push_back(r);
                opengl_obj.push_back(g);
                opengl_obj.push_back(b);
                opengl_obj.push_back(norm[0]);
                opengl_obj.push_back(norm[1]);
                opengl_obj.push_back(norm[2]);
                smooth_normals(&opengl_obj);
            }
        }
    }
    return opengl_obj;
}
