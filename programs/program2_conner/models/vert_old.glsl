#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNorm;

uniform mat4 rotateX;
uniform mat4 rotateY;
uniform mat4 rotateZ;
uniform mat4 scale;
uniform mat4 translate;
uniform mat4 model;
uniform mat4 itModel;

uniform mat4 perspective;
uniform mat4 camera;

uniform vec3 light;

out vec3 myColor;
out vec3 ourNormal;
out vec3 ourFragPos;
out vec3 lightPos;

vec4 pos;

mat4 m;

void main() {
    
    pos = perspective * camera * model * vec4(aPos,1.0);
    
    ourNormal = vec3(itModel) * aNorm;
    ourFragPos = vec3(model*vec4(aPos, 1.0f));
    myColor = aColor;
    lightPos = light;
    gl_Position = pos;
}
