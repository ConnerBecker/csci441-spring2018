#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <ctime>
#include <cmath>
#include <string.h>
#include <vector>

class Loader {
    public:
    Loader();
    std::vector<float> loadOBJFile(char*);
    void calc_normal(std::vector<float>,std::vector<float>,std::vector<float>,float*);
};

Loader::Loader() {
    std::cout << "Created Loader" << std::endl;
}

std::vector<float> Loader::loadOBJFile (char* fp) {
    std::vector<std::vector<float>> vertices;
    std::vector<std::vector<float>> normals;
    std::vector<std::vector<float> > points;
    std::vector<float> opengl_obj;
    std::fstream objFile(fp);
    std::string line;
    float r = 0.4f;
    float g = 0.5f;
    float b = 0.2f;
    
    while (std::getline(objFile,line)) {

        if (line[0] == 'v' && line[1] == ' ') {

            line = line.erase(0, line.find(" ")+1); // trim off the "v "
            
            std::vector<float> temp_v;
            for (int s = 0; s < 3; s++) {
            
                std::string l = line.substr(0, line.find(" "));
                temp_v.push_back(stof(l));
                line.erase(0, line.find(" ")+1);
            }
            vertices.push_back(temp_v);
        }
        
        if (line[0] == 'v' && line[1] == 'n') {

            line = line.erase(0, line.find(" ")+1); // trim off the "v "
            
            std::vector<float> temp_v;
            for (int s = 0; s < 3; s++) {
            
                std::string l = line.substr(0, line.find(" "));
                temp_v.push_back(stof(l));
                line.erase(0, line.find(" ")+1);
            }
            normals.push_back(temp_v);
        }
        
        if (line[0] == 'f' && line[1] == ' ') {
        
            int indexV[3];
            int indexN[3];
            std::vector<float> v1,v2,v3;
            
            line = line.erase(0, line.find(" ")+1);
            for (int s = 0; s < 3; s++) {
                std::string l = line.substr(0, line.find(" "));
                line.erase(0, line.find(" ")+1);
                for (int i = 0; i < 3; i++) {
                    int off = l.find("/");
                    indexV[i] = atoi(l.substr(0, off).c_str())-1;
                    l.erase(0, off+1);
                }
            }
            
            line = line.erase(0, line.find(" ")+1);
            for (int s = 0; s < 3; s++) {
                std::string l = line.substr(0, line.find(" "));
                line.erase(0, line.find(" ")+1);
                for (int i = 0; i < 3; i++) {
                    int off = l.find("/");
                    indexN[i] = atoi(l.substr(0, off).c_str())-1;
                    l.erase(0, off+1);
                }
            }
            
            
            for (int i = 0; i < 3; i++) {
                float x,y,z,n1,n2,n3;
                //r = (rand() % 50 + 50) / 100.0;
                //g = (rand() % 100) / 100.0;
                //b = (rand() % 100) / 100.0;
                r = r+=.001;
                
                x = vertices[indexV[i]-1][0];
                y = vertices[indexV[i]-1][1];
                z = vertices[indexV[i]-1][2];
                
                n1 = normals[indexN[i]-1][0];
                n2 = normals[indexN[i]-1][1];
                n3 = normals[indexN[i]-1][2];
                
                opengl_obj.push_back(x);
                opengl_obj.push_back(y);
                opengl_obj.push_back(z);
                opengl_obj.push_back(r);
                opengl_obj.push_back(g);
                opengl_obj.push_back(b);
                opengl_obj.push_back(n1);
                opengl_obj.push_back(n2);
                opengl_obj.push_back(n3);
            }
        }
    }
    std::cout << "All Done" << std::endl;
    return opengl_obj;
}
