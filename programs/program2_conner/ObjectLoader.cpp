#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <ctime>
#include <cmath>
#include <string.h>
#include <vector>
#include <map>

class Loader {
    public:
    Loader();
    float lastNorm[3];
    std::vector<float> loadOBJFile(char*);
    void calc_normal(std::vector<float>,std::vector<float>,std::vector<float>,float*);
    void smooth_normals(std::vector<float>*);
    float det(float, float, float, float);
    std::vector<float> loadMaze(char*);
    void addCube(std::vector<float>*, float, float);
};

void Loader::smooth_normals(std::vector<float>* v) {
    
    std::map<std::vector<float>, std::vector<float>> vectorMap;
    for (int i = 0; i < v->size(); i+=9) {
        std::vector<float> v_temp = {v->at(i), v->at(i+1), v->at(i+2)};
        if (vectorMap.count(v_temp) == 0) {
            vectorMap[v_temp] = {v->at(i+6), v->at(i+7), v->at(i+8)};
            for (int j=i+9;j<v->size();j+=9) {
                std::vector<float> check_vertex = {v->at(j), v->at(j+1), v->at(j+2)};
                if (v_temp == check_vertex) {
                    vectorMap[v_temp].push_back(v->at(j+6));
                    vectorMap[v_temp].push_back(v->at(j+7));
                    vectorMap[v_temp].push_back(v->at(j+8));
                }
            }
        }
    }
    
    for (std::map<std::vector<float>, std::vector<float>>::iterator it = vectorMap.begin();it != vectorMap.end(); it++) {
        std::vector<float> normals = it->second;

        float first = 0;
        float second = 0;
        float third = 0;
        for (int i=0;i<normals.size();i+=3) {

            first += normals[i];
            second += normals[i+1];
            third += normals[i+2];
        }
        first = first / (normals.size() / 3);
        second = second / (normals.size() / 3);
        third = third / (normals.size() / 3);
        
        for (int i=0;i<v->size();i+=9) {
            std::vector<float> check_vertex = {v->at(i), v->at(i+1), v->at(i+2)};
            if (check_vertex == it->first) {
                
                v->at(i+6) = first;
                v->at(i+7) = second;
                v->at(i+8) = third;
            }
        }
    }
}

float Loader::det(float a, float b, float c, float d) {
    return a*d - c*b;
}

void Loader::calc_normal(std::vector<float> v1, 
                         std::vector<float> v2, 
                         std::vector<float> v3, float* buf) {

    /* Calculates the normal vector for each of the fragments contained in the shape vector. The
     * two possible normals are calculated, then the normal with the larger distance from the center
     * of the shape. This allows accurate normals to be calculated for any solid shapes centered
     * about the origin, without hollow space in the center. Thus, this works for the sphere,
     * cylinder, and cube. */
    float t1[3] = {
                        det(v1[1], v1[2], v2[1], v2[2]),
                        -det(v1[0], v1[2], v2[0], v2[2]),
                        det(v1[0], v1[1], v2[0], v2[1])
                     };
    float t2[3] = {
                        det(v1[1], v1[2], v3[1], v3[2]),
                        -det(v1[0], v1[2], v3[0], v3[2]),
                        det(v1[0], v1[1], v3[0], v3[1])
                     };
    float d1 = sqrt(pow(t1[0]-lastNorm[0],2) + pow(t1[1]-lastNorm[1],2) + pow(t1[2]-lastNorm[2],2));
    float d2 = sqrt(pow(t2[0]-lastNorm[0],2) + pow(t2[1]-lastNorm[1],2) + pow(t2[2]-lastNorm[2],2));
    if (d1 < d2) {
        buf[0] =  t1[0];
        buf[1] =  t1[1];
        buf[2] =  t1[2];
        
        lastNorm[0] =  t1[0];
        lastNorm[1] =  t1[1];
        lastNorm[2] =  t1[2];
    } else {
        buf[0] =  t2[0];
        buf[1] =  t2[1];
        buf[2] =  t2[2];
        
        lastNorm[0] =  t2[0];
        lastNorm[1] =  t2[1];
        lastNorm[2] =  t2[2];
    }
}

/* The does not start facing the -z. This function rotates each point so the car
faces in -z*/
void fix_car_rotation(float* x, float* y, float* z) {
    // X
    float rotateX[16] = {1,0,0,0 ,0,1,0,0, 0,0,1,0 ,0,0,0,1};
    float rotateY[16] = {1,0,0,0 ,0,1,0,0, 0,0,1,0 ,0,0,0,1};
    float rotateZ[16] = {1,0,0,0 ,0,1,0,0, 0,0,1,0 ,0,0,0,1};
    
    float angleX = PI*-90.0/180.0f;
    rotateX[5] = cos(angleX);
    rotateX[6] = -sin(angleX);
    rotateX[9] = sin(angleX);
    rotateX[10] = cos(angleX);
    
    float oldY = *y;
    *x = *x;
    *y = *y * rotateX[5] + *z * rotateX[6];
    *z = oldY * rotateX[9] + *z * rotateX[10];
    
    // Y
    float angleY = PI*180/180.0f;
    rotateY[0] = cos(angleY);
    rotateY[2] = sin(angleY);
    rotateY[8] = -sin(angleY);
    rotateY[10] = cos(angleY);
    
    float oldX = *x;
    *x = *x * rotateY[0] + *z * rotateY[2];
    *y = *y;
    *z = oldX * rotateY[8] + *z * rotateY[10];

    // Z
    float angle = PI*360.0/180.0f;
    rotateZ[0] = cos(angle);
    rotateZ[1] = -sin(angle);
    rotateZ[4] = sin(angle);
    rotateZ[5] = cos(angle);
    
    oldX = *x;
    *x = *x * rotateZ[0] + *y * rotateZ[1];
    *y = oldX * rotateZ[4] + *y * rotateZ[5];
    *z = *z;
}

Loader::Loader() {
    std::cout << "Created Loader" << std::endl;
}

/* This function loads in the car obj file*/
std::vector<float> Loader::loadOBJFile (char* fp) {
    std::vector<std::vector<float>> vertices;
    std::vector<std::vector<float>> normals;
    std::vector<std::vector<float> > points;
    std::vector<float> opengl_obj;
    std::fstream objFile(fp);
    std::string line;
    float r = 0.8f;
    float g = 0.8f;
    float b = 0.8f;
    float SCALE = 0.05f;
    bool carFlag = false;
    std::string pathCheck(fp);
    
    std::cout << pathCheck.find("../models/maze.obj") << std::endl;
    if (pathCheck.find("../models/maze.obj") == 0) {
        SCALE = 1.0f;
    }
    else {
        carFlag = true;
    }
    
    while (std::getline(objFile,line)) {

        if (line[0] == 'v' && line[1] == ' ') {

            line = line.erase(0, line.find(" ")+1); // trim off the "v "
            
            std::vector<float> temp_v;
            for (int s = 0; s < 3; s++) {
            
                std::string l = line.substr(0, line.find(" "));
                temp_v.push_back(stof(l));
                line.erase(0, line.find(" ")+1);
            }
            vertices.push_back(temp_v);
        }

        if (line[0] == 'v' && line[1] == 'n') {

            line = line.erase(0, line.find(" ")+1); // trim off the "vn "
            
            std::vector<float> temp_v;
            for (int s = 0; s < 3; s++) {
            
                std::string l = line.substr(0, line.find(" "));
                temp_v.push_back(stof(l));
                line.erase(0, line.find(" ")+1);
            }
            normals.push_back(temp_v);
        }

        if (line[0] == 'f' && line[1] == ' ') {
        
            int indexV[3];
            int indexN[3];
            std::vector<float> v1,v2,v3;
            
            line = line.erase(0, line.find(" ")+1); // trim off the f
            for (int s = 0; s < 3; s++) {
                std::string l = line.substr(0, line.find(" "));
                line.erase(0, line.find(" ")+1);
                for (int i = 0; i < 3; i++) {
                    if (i == 0) {
                        int off = l.find("/");
                        indexV[s] = atoi(l.substr(0, off).c_str())-1;
                        l.erase(0, off+1);
                    } else if (i == 1) {
                        int off = l.find("/");
                        l.erase(0, off+1);
                    } else {
                        int off = l.find("/");
                        indexN[s] = atoi(l.substr(0, off).c_str())-1;
                        l.erase(0, off+1);
                    }
                }
            }
            
            
            /* loaded normals code */
            for (int i = 0; i < 3; i++) {
                float x,y,z,n1,n2,n3;
                //r = r+=.00001;

                x = vertices[indexV[i]][0];
                y = vertices[indexV[i]][1];
                z = vertices[indexV[i]][2];
                if (carFlag) {
                    fix_car_rotation(&x, &y, &z);
                }
                n1 = normals[indexN[i]][0];
                n2 = normals[indexN[i]][1];
                n3 = normals[indexN[i]][2];

                opengl_obj.push_back(x*SCALE);
                opengl_obj.push_back(y*SCALE);
                opengl_obj.push_back(z*SCALE);
                opengl_obj.push_back(r);
                opengl_obj.push_back(g);
                opengl_obj.push_back(b);
                opengl_obj.push_back(n1*SCALE);
                opengl_obj.push_back(n2*SCALE);
                opengl_obj.push_back(n3*SCALE);
            }
        }
    }
    std::cout << "All Done" << std::endl;
    return opengl_obj;
}

/* Addes a cube to vector whith approriate offsets when a # is found in maze
file */
void Loader::addCube(std::vector<float>* maze, float offsetX, float offsetZ) {
    
    float r,g,b;
    r = 0.1f;
    g = 0.1f;
    b = rand() % 50 / 100 + 0.49f;
    float cube[36*9] = {
                              0.0f, 0.0f, 0.0f, r, g, b, 0.0f, 0.0f, 1.0f,
                              0.5f, 0.0f, 0.0f, r, g, b, 0.0f, 0.0f, 1.0f,
                              0.5f, 0.5f, 0.0f, r, g, b, 0.0f, 0.0f, 1.0f,
                              0.0f, 0.0f, 0.0f, r, g, b, 0.0f, 0.0f, 1.0f, // front
                              0.0f, 0.5f, 0.0f, r, g, b, 0.0f, 0.0f, 1.0f,
                              0.5f, 0.5f, 0.0f, r, g, b, 0.0f, 0.0f, 1.0f,
                              
                              0.0f, 0.0f, -0.5f, r, g, b, 0.0f, 0.0f, -1.0f,
                              0.5f, 0.0f, -0.5f, r, g, b, 0.0f, 0.0f, -1.0f,
                              0.5f, 0.5f, -0.5f, r, g, b, 0.0f, 0.0f, -1.0f,
                              0.0f, 0.0f, -0.5f, r, g, b, 0.0f, 0.0f, -1.0f, // back
                              0.0f, 0.5f, -0.5f, r, g, b, 0.0f, 0.0f, -1.0f,
                              0.5f, 0.5f, -0.5f, r, g, b, 0.0f, 0.0f, -1.0f,
                              
                              0.0f, 0.5f,  0.0f,  r, g, b,  0.0f,  1.0f, 0.0f,
                              0.5f, 0.5f,  0.0f,  r, g, b,  0.0f,  1.0f, 0.0f,
                              0.5f, 0.5f, -0.5f,  r, g, b,  0.0f,  1.0f, 0.0f,
                              0.0f, 0.5f,  0.0f,  r, g, b,  0.0f,  1.0f, 0.0f, // top
                              0.0f, 0.5f, -0.5f,  r, g, b,  0.0f,  1.0f, 0.0f,
                              0.5f, 0.5f, -0.5f,  r, g, b,  0.0f,  1.0f, 0.0f,
                              
                              0.0f, 0.0f,  0.0f, r, g, b, 0.0f, -1.0f, 0.0f,
                              0.5f, 0.0f,  0.0f, r, g, b, 0.0f, -1.0f, 0.0f,
                              0.5f, 0.0f, -0.5f, r, g, b, 0.0f, -1.0f, 0.0f, // bottom
                              0.0f, 0.0f,  0.0f, r, g, b, 0.0f, -1.0f, 0.0f,
                              0.0f, 0.0f, -0.5f, r, g, b, 0.0f, -1.0f, 0.0f,
                              0.5f, 0.0f, -0.5f, r, g, b, 0.0f, -1.0f, 0.0f,
                              
                              0.0f, 0.0f,  0.0f, r, g, b, -1.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, -0.5f, r, g, b, -1.0f, 0.0f, 0.0f,
                              0.0f, 0.5f, -0.5f, r, g, b, -1.0f, 0.0f, 0.0f, // left
                              0.0f, 0.0f,  0.0f, r, g, b, -1.0f, 0.0f, 0.0f,
                              0.0f, 0.5f,  0.0f, r, g, b, -1.0f, 0.0f, 0.0f,
                              0.0f, 0.5f, -0.5f, r, g, b, -1.0f, 0.0f, 0.0f,
                              
                              0.5f, 0.0f,  0.0f, r, g, b, 1.0f, 0.0f, 0.0f,
                              0.5f, 0.0f, -0.5f, r, g, b, 1.0f, 0.0f, 0.0f,
                              0.5f, 0.5f, -0.5f, r, g, b, 1.0f, 0.0f, 0.0f, // right
                              0.5f, 0.0f,  0.0f, r, g, b, 1.0f, 0.0f, 0.0f,
                              0.5f, 0.5f,  0.0f, r, g, b, 1.0f, 0.0f, 0.0f,
                              0.5f, 0.5f, -0.5f, r, g, b, 1.0f, 0.0f, 0.0f
                              };
    
    for (int i = 0; i < 36*9; i+=9) {
        maze->push_back(cube[i+0]+offsetX);
        maze->push_back(cube[i+1]);
        maze->push_back(cube[i+2]+offsetZ);
        maze->push_back(cube[i+3]);
        maze->push_back(cube[i+4]);
        maze->push_back(cube[i+5]);
        maze->push_back(cube[i+6]);
        maze->push_back(cube[i+7]);
        maze->push_back(cube[i+8]);
    }
}

/* builds maze from # and 0 formatted file */
std::vector<float> Loader::loadMaze(char* fp) {
    std::fstream mazeOBJ(fp);
    std::string maze_line;
    
    std::vector<float> myMaze;
    
    float xOff = 0.5f;
    float zOff = 0;
    while (std::getline(mazeOBJ,maze_line)) {
        for (int i = 0; i < maze_line.length(); i++) {
            if (maze_line[i] == '#') {
                addCube(&myMaze, xOff*i, zOff);
            }
        }
        zOff -=0.5f;
    }
    
    return myMaze;
}


































