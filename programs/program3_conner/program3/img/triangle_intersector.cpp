#include <iostream>
#include <vector>
#include <stdlib.h>


float determinant3x3(float* a) {
    
    float part1 = a[0] * ( a[4] * a[8] - a[5] * a[7] );
    float part2 = a[1] * ( a[3] * a[8] - a[5] * a[6] );
    float part3 = a[2] * ( a[3] * a[7] - a[4] * a[6] );
    
    return part1 - part2 - part3;
}

float triangle_intersect(float* a, float* b, float* c, float* origin, float* direction) {
    
    float base_matrix[9] = { b[0] - a[0],  c[0] - a[0],    -direction[0],
                             b[1] - a[1],  c[1] - a[1],    -direction[1],
                             b[2] - a[2],  c[2] - a[2],    -direction[2]
                           };
    float base_det = determinant3x3(base_matrix);


    float u_matrix[9] = { origin[0] - a[0],    c[0] - a[0],    -direction[0],
                           origin[1] - a[1],    c[1] - a[1],    -direction[1],
                           origin[2] - a[2],    c[2] - a[2],    -direction[2]
                         };
    float u = determinant3x3(u_matrix)/ base_det;
    
    float v_matrix[9] = { b[0] - a[0],    origin[0] - a[0],   -direction[0],
                           b[1] - a[1],    origin[1] - a[1],   -direction[1],
                           b[2] - a[2],    origin[2] - a[2],   -direction[2]
                         };
    float v = determinant3x3(v_matrix)/base_det;
    
    float t_matrix[9] = { b[0] - a[0],    c[0] - a[0],    origin[0] - a[0],
                           b[1] - a[1],    c[1] - a[1],    origin[1] - a[1],
                           b[2] - a[2],    c[2] - a[2],    origin[2] - a[2]
                         };
    float t = determinant3x3(t_matrix)/base_det;

    if (v < 0 || v > 1) {return -1;}
    if (u < 0 || 1-v<u) {return -2;}
    if (t < 0)          {return -3;}
    
    return t;
}

int main() {
    float a[3] = {-1,-1,2};
    float b[3] = {1,-1,2};
    float c[3] = {0,1,2};
    
    float e[3] = {0,0,0};
    float d[3] = {0,0,-1};
    
    float t = triangle_intersect(a,b,c,e,d);
    
    if (t >=0) {
        float ans[3] = {e[0] + t*d[0],
                     e[1] + t*d[1],
                     e[2] + t*d[2]};
        std::cout<<"Inter at: "<< ans[0]<<", "<<ans[1]<<", "<<ans[2]<<std::endl;
    } else {
        std::cout<<"No Inter."<<std::endl;
    }    
}
