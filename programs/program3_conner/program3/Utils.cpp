#include <vector>


/* takes the determinant of a 3x3 matrix represented as aflat array. */
float determinant3x3(float* a) {
    
    float part1 = a[0] * ( a[4] * a[8] - a[5] * a[7] );
    float part2 = a[1] * ( a[3] * a[8] - a[5] * a[6] );
    float part3 = a[2] * ( a[3] * a[7] - a[4] * a[6] );
    
    return part1 - part2 + part3;
}

float max(float a, float b) {
    
    return a >= b ? a : b;
}

float clamp(float val, float c) {
    return val > c ? c : val;
}


/* multiplies two 4x4 matrices represented as flat arrays */
void multMatrix4x4(float* b2, float* b1) {
    float temp[16];
    for(int r = 0; r < 4; r++) {
        for(int c = 0; c < 4; c++) {
            temp[r*4+c] = b1[r*4]*b2[c] + b1[r*4+1]*b2[c+4] + b1[r*4+2]*b2[c+8] + b1[r*4+3]*b2[c+12];
        }
    }
    for(int i = 0; i < 16; i++) {
        b2[i] = temp[i];
    }
}

/* rotates a 4x4 matrix around the y axis */
void rotateY(float angle, float* buffer) {
    buffer[0] = (float) cos(angle);
    buffer[1] = 0.0f;
    buffer[2] = (float) sin(angle);
    buffer[3] = 0.0f;
    buffer[4] = 0.0f;
    buffer[5] = 1.0f;
    buffer[6] = 0.0f;
    buffer[7] = 0.0f;
    buffer[8] = (float) -sin(angle);
    buffer[9] = 0.0f;
    buffer[10] = (float) cos(angle);
    buffer[11] = 0.0f;
    buffer[12] = 0.0f;
    buffer[13] = 0.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

void loadIdentity4x4(float* buffer) {
    for(int i = 0; i < 16; i++) {
        buffer[i] = 0.0f;
    }
    buffer[0] = 1.0f;
    buffer[5] = 1.0f;
    buffer[10] = 1.0f;
    buffer[15] = 1.0f;
}

/* subtracts a n-dimensionsal vector b from n-dimenstional a */
float* subtractVec(float* a, float* b, int ele) {
    float* newVec = new float[ele];
    for (int i = 0; i < ele; i++) {
        newVec[i] = a[i] - b[i];
    }
    
    return newVec;
}


/* takes the dot product of two n-dimensional vectors */
float dotVec(float* a, float* b, int ele) {
    float ret = 0;
    for (int i = 0; i < ele; i++) {
        ret += (a[i] * b[i]);
    }
    
    return ret;
}


/* returns the length of a n-dimensional vector */
float distanceVec(float* a, int ele) {
    float ret = 0;
    for (int i = 0; i < ele; i++) {
        ret += (a[i]*a[i]);
    }
    
    ret = sqrt(ret);
    return ret;
}

/* returns the distance between two n-dimensional vectors */
float distanceVec(float* a, float* b, int ele) {
    float ret = 0;
    for (int i = 0; i < ele; i++) {
        ret += (a[i]-b[i])*(a[i]-b[i]);
    }
    
    ret = sqrt(ret);
    return ret;
}

void setVec(float* buf, float* data, int offset, int ele) {
    for (int i = 0; i < ele; i++) {
        buf[i] = data[i+offset];
    }
}

void setVec(float* buf, std::vector<float> data, int offset, int ele) {
    for (int i = 0; i < ele; i++) {
        buf[i] = data[i+offset];
    }
}

void normalize(float* v, int sz) {
    float total = 0;
    for (int i = 0; i < sz; i++) {
        total = total + pow(v[i],2);
    }
    total = sqrt(total);
    for (int i = 0; i < sz; i++) {
        v[i] = v[i]/total;
    }
}

/* finds the intersection time between a vector represented as an origin and
   directions with a triangle represented as 3 3D points */
float intersectTriangleVec(float* a, float* b, float* c, 
                           float* origin, float* direction) {
    
    float base_matrix[9] = { b[0] - a[0],  c[0] - a[0],    -direction[0],
                             b[1] - a[1],  c[1] - a[1],    -direction[1],
                             b[2] - a[2],  c[2] - a[2],    -direction[2]
                           };
    float base_det = determinant3x3(base_matrix);


    float u_matrix[9] = { origin[0] - a[0],    c[0] - a[0],    -direction[0],
                          origin[1] - a[1],    c[1] - a[1],    -direction[1],
                          origin[2] - a[2],    c[2] - a[2],    -direction[2]
                         };
    float u = determinant3x3(u_matrix)/ base_det;
    
    float v_matrix[9] = { b[0] - a[0],    origin[0] - a[0],   -direction[0],
                          b[1] - a[1],    origin[1] - a[1],   -direction[1],
                          b[2] - a[2],    origin[2] - a[2],   -direction[2]
                         };
    float v = determinant3x3(v_matrix)/base_det;
    
    float t_matrix[9] = { b[0] - a[0],    c[0] - a[0],    origin[0] - a[0],
                          b[1] - a[1],    c[1] - a[1],    origin[1] - a[1],
                          b[2] - a[2],    c[2] - a[2],    origin[2] - a[2]
                         };
    float t = determinant3x3(t_matrix)/base_det;

    if (v < 0 || v > 1) {return -1;}
    if (u < 0 || 1-v<u) {return -2;}
    if (t < 0)          {return -3;}

    return t;
}

/* finds the intersection time between a sphere represented as a radius and a
   center with a vector represented as an origin and a direction. */
float intersectSphereVec(const float* center, float radius, float* origin, float* direction) {
    float omc[3] = {origin[0] - center[0],
                    origin[1] - center[1],
                    origin[2] - center[2]
                    };
    float A = direction[0]*direction[0] + 
              direction[1]*direction[1] + 
              direction[2]*direction[2];
    float B = (2.0f*direction[0]) * omc[0] +
              (2.0f*direction[1]) * omc[1] +
              (2.0f*direction[2]) * omc[2];
    float C = omc[0] * omc[0] +
              omc[1] * omc[1] +
              omc[2] * omc[2] -
              radius * radius ;
    float D = (B*B) - 4.0f*A*C;
    float q = 0;
    if(D<=0.0){return -1;}
 
    float t1 = ( -B + sqrt( (B*B)-4*A*C ) ) / (2*A);
    float t2 = ( -B - sqrt( (B*B)-4*A*C ) ) / (2*A);

    if (t1 >= 0 && t2 >= 0 && t2 < t1) {
        return t2;
    } else if (t2 >= 0 && t1 >= 0 && t1 < t2) {
        return t1;
    } else {
        return -1;
    }
}
