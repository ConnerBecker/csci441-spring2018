//https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/Areacentric-coordinates

#include <cmath>
#include "bitmap_image.hpp"
#ifndef UTILS_CPP
#define UTILS_CPP
#include "Utils.cpp"
#endif


/* takes the cross product of two 3D vectors and returns the length of the 
   resulting vector */
float crossLength(float* a, float* b) {
    float u[3] = { a[1]*b[2] - a[2]*b[1],
                   a[2]*b[0] - a[0]*b[2],
                   a[0]*b[1] - a[1]*b[0] };
    float res = distanceVec(u,3);
    return res;
}

/* finds the area of a triangle represented by 3 3D points */
float getTriangleArea(float* a, float* b, float* c) {
    float* bma = subtractVec(b,a,3);
    float* cma = subtractVec(c,a,3);
    return (crossLength(bma,cma)/2.0f);
}

/* This function performs phong shading on a sphere. It assumes the sphere is 
   composed of triangles and that a single triangle is passed to it along with 
   the intersection of the triangle and a ray, the light position, camera 
   position, original color, and p value */
rgb_t sphereRayPhong(float* center, float* intersection, 
                     float* light_pos, float* cam, float* s_color, 
                     float p, float* a, float* b, float* c) {

    float light_c[3] = {0.3f,0.3f,0.3f};
    float ambient = .4;
    
    float* light = subtractVec(light_pos, intersection, 3);
    
    /* finding vertex normals */
    float* ac = subtractVec(a, center, 3);
    float* bc = subtractVec(b, center, 3);
    float* cc = subtractVec(c, center, 3);
    
    /* finding the coefficients used in barycentric coordinates. This will be 
       used to determine how much of each vertex normal to use */
    float w = getTriangleArea(b,c,intersection) / getTriangleArea(a,b,c);
    float u = getTriangleArea(c,a,intersection) / getTriangleArea(a,b,c);
    float v = getTriangleArea(a,b,intersection) / getTriangleArea(a,b,c);
    
    /* finding the intersection normal*/
    float norm[3] = {w*ac[0] + u*bc[0] + v*cc[0],
                     w*ac[1] + u*bc[1] + v*cc[1],
                     w*ac[2] + u*bc[2] + v*cc[2]};

    normalize(norm,3);
    
    float l_dot_n = dotVec(light, norm, 3);

    float r[3] = {-light[0] + 2*l_dot_n*norm[0],
                  -light[1] + 2*l_dot_n*norm[1],
                  -light[2] + 2*l_dot_n*norm[2]};
    normalize(r,3);
    
    float* e = subtractVec(cam, intersection, 3);
    normalize(e,3);
    
    float r_dot_e = dotVec(r, e, 3);
                       
    float diffuse[3] = { s_color[0]*(clamp(ambient + light_c[0]*max(0,l_dot_n),1)),
                         s_color[1]*(clamp(ambient + light_c[1]*max(0,l_dot_n),1)),
                         s_color[2]*(clamp(ambient + light_c[2]*max(0,l_dot_n),1))};

    float spec[3] = { light_c[0]*(float)pow(max(0,r_dot_e),p),
                      light_c[1]*(float)pow(max(0,r_dot_e),p),
                      light_c[2]*(float)pow(max(0,r_dot_e),p)
                    };
                                     
    rgb_t phong_color = make_colour( clamp((spec[0]+diffuse[0]),1) * 255,
                                     clamp((spec[1]+diffuse[1]),1) * 255,
                                     clamp((spec[2]+diffuse[2]),1) * 255 );
        
    return phong_color;
}
