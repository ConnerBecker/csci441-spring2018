#include <vector>
#include <math.h>
#include <algorithm>
#include <cstdlib>

#ifndef UTILS_CPP
#define UTILS_CPP
#include "Utils.cpp"
#endif

#define PI 3.141592654
#define S_QUALITY 50

class Ray {
    public:
    float origin[3];
    float direction[3];
    float x;
    float y;

    // Constructor
    Ray();
    Ray(float, float, float, float, 
        float, float, float, float, 
        float*, float*, float*, float*, float);
        
    void setOriginPersp(float*);
    void calculateDirectionPersp(float, float, float, float*, float*, float*);
    float intersectSphere(float*, float);
    float intersectTrianglePersp(float*, float*, float*);
    ~Ray() {
        //std::cout << "Destroyed Ray" << std::endl;
    }
};

Ray::Ray() {
    // created ray
}

// this constructor creates a ray and populates its perspective viewing
Ray::Ray(float l, float r, float b, float t, 
         float i, float j, float nX, float nY,
         float* w, float* u, float* v, float* cam, float d) {
    x = i;
    y = j;
    float uR = l + (r - l) * (i + 0.5f)/nX;
    float vR = t + (b - t) * (j + 0.5f)/nY;
    
    setOriginPersp(cam);                            // used for perspective
    calculateDirectionPersp(uR, vR, d, u, v, w);    // used for perspective
    normalize(direction,3);
}

/* calculates the ray direction for perspective viewing based on -dw + uU + vV */
void Ray::calculateDirectionPersp(float uR, float vR, float d, float* u, float* v, float* w) {
    direction[0] = -d*w[0] + uR*u[0] + vR*v[0];
    direction[1] = -d*w[1] + uR*u[1] + vR*v[1];
    direction[2] = -d*w[2] + uR*u[2] + vR*v[2];
}

void Ray::setOriginPersp(float* o) {
    origin[0] = o[0]; origin[1] = o[1]; origin[2] = o[2];
}

/* this function finds the intersection of a ray and a triangle represented as 
   3 3D points. Cramer's rule is used to solve for u,v,t based on the matrices
   provided by Dr.Millman in class */
float Ray::intersectTrianglePersp(float* a, float* b, float* c) {
    
    float base_matrix[9] = { b[0] - a[0],  c[0] - a[0],    -direction[0],
                             b[1] - a[1],  c[1] - a[1],    -direction[1],
                             b[2] - a[2],  c[2] - a[2],    -direction[2]
                           };
    float base_det = determinant3x3(base_matrix);


    float u_matrix[9] = { origin[0] - a[0],    c[0] - a[0],    -direction[0],
                          origin[1] - a[1],    c[1] - a[1],    -direction[1],
                          origin[2] - a[2],    c[2] - a[2],    -direction[2]
                         };
    float u = determinant3x3(u_matrix)/ base_det;
    
    float v_matrix[9] = { b[0] - a[0],    origin[0] - a[0],   -direction[0],
                          b[1] - a[1],    origin[1] - a[1],   -direction[1],
                          b[2] - a[2],    origin[2] - a[2],   -direction[2]
                         };
    float v = determinant3x3(v_matrix)/base_det;
    
    float t_matrix[9] = { b[0] - a[0],    c[0] - a[0],    origin[0] - a[0],
                          b[1] - a[1],    c[1] - a[1],    origin[1] - a[1],
                          b[2] - a[2],    c[2] - a[2],    origin[2] - a[2]
                         };
    float t = determinant3x3(t_matrix)/base_det;

    if (v < 0 || v > 1) {return -1;}
    if (u < 0 || 1-v<u) {return -2;}
    if (t < 0)          {return -3;}

    return t;
}





/******************************************************************************/
//                  Millman's Sphere                                          //
/******************************************************************************/



class Sphere_Mesh {
    float x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    float y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

    float z(float r, float phi, float theta){
        return r*cos(phi);
    }

public:
    float center[3];
    float color[3];
    float radius;
    std::vector<std::vector<float>> triangles;
    
    bool operator ==(Sphere_Mesh a) const {
        if (a.radius == radius &&
        a.center[0] == center[0] && 
        a.center[1] == center[1] && 
        a.center[2] == center[2]) {
        
        return true;
        }else{return false;}
    };
    
    Sphere_Mesh(unsigned int, float*, float, float*);
};

Sphere_Mesh::Sphere_Mesh(unsigned int n, float* cent, float rad, float* col) {
        
        radius = rad;
        center[0] = cent[0]; center[1] = cent[1]; center[2] = cent[2];
        color[0]  =  col[0];  color[1] =  col[1]; color[2]  =  col[2];
        
        int n_steps = (n%2==0) ? n : n+1;
        float step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps; ++i) {
            for (int j = 0; j < n_steps/2.0; ++j) {
                float phi_i = i*step_size;
                float phi_ip1 = ((i+1)%n_steps)*step_size;
                float theta_j = j*step_size;
                float theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                float vij_x = x(radius, phi_i, theta_j);
                float vij_y = y(radius, phi_i, theta_j);
                float vij_z = z(radius, phi_i, theta_j);

                // vertex i+1,j
                float vip1j_x = x(radius, phi_ip1, theta_j);
                float vip1j_y = y(radius, phi_ip1, theta_j);
                float vip1j_z = z(radius, phi_ip1, theta_j);

                // vertex i,j+1
                float vijp1_x = x(radius, phi_i, theta_jp1);
                float vijp1_y = y(radius, phi_i, theta_jp1);
                float vijp1_z = z(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                float vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                float vip1jp1_y = y(radius, phi_ip1, theta_jp1);
                float vip1jp1_z = z(radius, phi_ip1, theta_jp1);

                std::vector<float> tri, tri2;
                tri.push_back(vij_x);
                tri.push_back(vij_y);
                tri.push_back(vij_z);
                
                tri.push_back(vip1j_x);
                tri.push_back(vip1j_y);
                tri.push_back(vip1j_z);
                
                tri.push_back(vijp1_x);
                tri.push_back(vijp1_y);
                tri.push_back(vijp1_z);
                // add triangle
                
                tri2.push_back(vijp1_x);
                tri2.push_back(vijp1_y);
                tri2.push_back(vijp1_z);
                
                tri2.push_back(vip1jp1_x);
                tri2.push_back(vip1jp1_y);
                tri2.push_back(vip1jp1_z);
                
                tri2.push_back(vip1j_x);
                tri2.push_back(vip1j_y);
                tri2.push_back(vip1j_z);
                // add triange
                
                triangles.push_back(tri);
                triangles.push_back(tri2);

            }
        }
        for (int i = 0; i < triangles.size(); i++) {
            for(int j = 0; j < 9; j++) {
                triangles[i][j] += center[j%3];
            }
        }
}



/******************************************************************************/
//                  Conner's Sphere                                          //
/******************************************************************************/



/*class Sphere_Mesh {
    public:
    std::vector<std::vector<float>> triangles;
    float center[3];
    float color[3];
    float radius;
    Sphere_Mesh();
    Sphere_Mesh(float*, float, float*);
    void generate_triangles(int);
};

Sphere_Mesh::Sphere_Mesh(float* cent, float rad, float* col) {

    radius = rad;
    center[0] = cent[0]; center[1] = cent[1]; center[2] = cent[2];
    color[0] = col[0]; color[1] = col[1]; color[2] = col[2];
    generate_triangles(rad*15);
}

void Sphere_Mesh::generate_triangles(int n) {
    std::cout<<"n "<<n<<std::endl;
    float increment = 2.0f*PI/(float)n;         // full revolution of unit circle is 2PI

    float circle[n][4];
    for(int i = 0; i < n; i++){ 
        circle[i][0] = cos(i*increment);
        circle[i][1] = sin(i*increment);
        circle[i][2] = 0.0f;
        circle[i][3] = 0.0f;
    }
    for(int circ_c = 0; circ_c < n/2; circ_c++){
        float angle1 = (float)(circ_c/(float)n*360);
        angle1 = angle1*PI/180.0f;
        float angle2 = (float)((circ_c+1)/(float)n*360);
        angle2 = angle2*PI/180.0f;
        float angleStorage[16];
        for(int i = 0; i < n; i++) {
            float xyz1[16] = {0.0f,0.0f,0.0f,circle[i][0],0.0f,0.0f,0.0f,circle[i][1],0.0f,0.0f,0.0f,circle[i][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle1, angleStorage); multMatrix4x4(xyz1, angleStorage); std::fill_n(angleStorage,16,0);
            float xyz2[16] = {0.0f,0.0f,0.0f,circle[(i+1)%n][0],0.0f,0.0f,0.0f,circle[(i+1)%n][1],0.0f,0.0f,0.0f,circle[(i+1)%n][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle1, angleStorage); multMatrix4x4(xyz2, angleStorage); std::fill_n(angleStorage,16,0);
            float xyz3[16] = {0.0f,0.0f,0.0f,circle[i][0],0.0f,0.0f,0.0f,circle[i][1],0.0f,0.0f,0.0f,circle[i][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle2, angleStorage); multMatrix4x4(xyz3, angleStorage); std::fill_n(angleStorage,16,0);
            float xyz4[16] = {0.0f,0.0f,0.0f,circle[(i+1)%n][0],0.0f,0.0f,0.0f,circle[(i+1)%n][1],0.0f,0.0f,0.0f,circle[(i+1)%n][2],0.0f,0.0f,0.0f,0.0f}; 
            rotateY(angle2, angleStorage); multMatrix4x4(xyz4, angleStorage); std::fill_n(angleStorage,16,0);
            
            std::vector<float> temp_tri, temp_tri2;
            
            temp_tri.push_back(xyz1[3]); temp_tri.push_back(xyz1[7]); temp_tri.push_back(xyz1[11]);
            temp_tri.push_back(xyz2[3]); temp_tri.push_back(xyz2[7]); temp_tri.push_back(xyz2[11]);
            temp_tri.push_back(xyz3[3]); temp_tri.push_back(xyz3[7]); temp_tri.push_back(xyz3[11]);
            
            temp_tri2.push_back(xyz2[3]); temp_tri2.push_back(xyz2[7]); temp_tri2.push_back(xyz2[11]);
            temp_tri2.push_back(xyz3[3]); temp_tri2.push_back(xyz3[7]); temp_tri2.push_back(xyz3[11]);
            temp_tri2.push_back(xyz4[3]); temp_tri2.push_back(xyz4[7]); temp_tri2.push_back(xyz4[11]);
            
            triangles.push_back(temp_tri);
            triangles.push_back(temp_tri2);
            
        }
    }
    
    for (int i = 0; i < triangles.size(); i++) {
        for(int j = 0; j < 9; j++) {
            triangles[i][j] = triangles[i][j] * radius;
            triangles[i][j] += center[j%3];
        }
    }
}
*/

























