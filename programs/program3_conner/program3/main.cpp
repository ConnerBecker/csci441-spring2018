#include <iostream>
#include <vector>
#include <time.h>
#include <omp.h>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"
#include "Primitives.cpp"
#include "Shading.cpp"

#define WIDTH 500.0
#define HEIGHT 500.0
#define P 10
#define D 2.0

float light[3] = {8,4,3};

std::vector<Ray> rays;

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

void calculateU(float* w, float* up, float* buf) {
    normalize(w, 3);
    float u[3] = { up[1]*w[2] - up[2]*w[1],
                   up[2]*w[0] - up[0]*w[2],
                   up[0]*w[1] - up[1]*w[0] };
    normalize(u,3);
    buf[0] = u[0];
    buf[1] = u[1];
    buf[2] = u[2];
}

void calculateV(float* w, float* u, float* buf) {
    normalize(w, 3);
    float v[3] = { w[1]*u[2] - w[2]*u[1],
                   w[2]*u[0] - w[0]*u[2],
                   w[0]*u[1] - w[1]*u[0] };
    normalize(v,3);
    buf[0] = v[0];
    buf[1] = v[1];
    buf[2] = v[2];
}

void colorBitmap(bitmap_image* img, rgb_t* color) {
    for (int x = 0; x < WIDTH; x++) {
        for (int y = 0; y < HEIGHT; y++) {
            img->set_pixel(x,y,*color);
        }
    }
}

//this function iterates over every pixel in an image an creates a ray for it
// Ray Params:
// l,r = left, right
// b,t = top, bottom
// i,j = pixel (x,y)
// nX = image width
// nY = image height
// w,u,v = w,u,v float matrices
// cam = camera origin
void createRays(Viewport view, float* w, float* u, float* v, float* cam) {
    float l = view.min[0];
    float r = view.max[0];
    float b = view.min[1];
    float t = view.max[1];
    for (int x = 0; x < WIDTH; x++) {
        for (int y = 0; y < HEIGHT; y++) {
            rays.push_back(Ray(l,r,b,t,x,y,WIDTH, HEIGHT, w, u, v, cam, D) );
        }
    }
}

void renderPerspWithPhong(bitmap_image& image, const std::vector<Sphere_Mesh>& world, float* cam) {
    
    /* iterate over every area and parallelize the process with the CPU */
    #pragma omp parallel for
        for (int i = 0; i < rays.size(); i++) {
            
            /* for iterate over each shape in the world for the current ray */
            float smallest_t = 9999;
            for (int w = 0; w < world.size(); w++) {
                
                /* iterate over each triangle that composes the shape and check 
                   if the current ray intersects it */
                for(int tri = 0; tri < world[w].triangles.size(); tri++) {
                    float a[3] = {world[w].triangles[tri][0],
                                  world[w].triangles[tri][1],
                                  world[w].triangles[tri][2]};
                    float b[3] = {world[w].triangles[tri][3],
                                  world[w].triangles[tri][4],
                                  world[w].triangles[tri][5]};
                    float c[3] = {world[w].triangles[tri][6],
                                  world[w].triangles[tri][7],
                                  world[w].triangles[tri][8]};
                    float t_val = rays[i].intersectTrianglePersp(a,b,c);
                    if (t_val >= 0 && t_val < smallest_t) {
                        
                        /* upon successful intersection, time >= 0, calcuate 
                           intersection and color iamge accordingly. */
                        smallest_t = t_val;
                        float center[3] = {world[w].center[0],
                                           world[w].center[1],
                                           world[w].center[2]};
                        float inter[3] = {rays[i].origin[0] + smallest_t*rays[i].direction[0],
                                          rays[i].origin[1] + smallest_t*rays[i].direction[1],
                                          rays[i].origin[2] + smallest_t*rays[i].direction[2]};

                        float s_color[3] = {world[w].color[0]/255.0f,
                                            world[w].color[1]/255.0f,
                                            world[w].color[2]/255.0f};
                        
                        /* the below code is used to find if the current 
                           intersection falls inside a shadow */                 
                        float shadow = -1;
                        float new_or[3] = {inter[0], inter[1], inter[2]};
                        float* new_dir = subtractVec(light, inter, 3);
                        normalize(new_dir,3);
                        for (int shad = 0; shad < world.size(); shad++) {
                        
                            float time = intersectSphereVec(world[shad].center,
                                                            world[shad].radius,                          // First the ray points from the intersection to the light
                                                            new_or,                                      // is calculated. Then this ray is intersected with every
                                                            new_dir);                                    // shape in the world. This is done using only the shape
                            shadow = (shadow<0 || (time > shadow && time >= 0)) ? time : shadow;         // descriptors since intersecting the ray with each triangle
                        }                                                                                // is not neccessary. If a intersection is found then it is 
                        if (shadow >= 0){                                                                // assumed the intersection that was originally found is in
                            float w_inter[3] = {new_or[0] + shadow*new_dir[0],                           // a shadow. The ratio of distance to shape casting shadow 
                                                new_or[1] + shadow*new_dir[1],                           // and distance to light source from intersection is used to
                                                new_or[2] + shadow*new_dir[2]};                          // determine how dark the shadow should be.
                            float ratio = distanceVec(inter,w_inter,3)/distanceVec(inter,light,3);
                            rgb_t color = sphereRayPhong(center, inter, light, cam, s_color, P, a,b,c);
                            color.red *= ratio;
                            color.green *= ratio;
                            color.blue *= ratio;
                            image.set_pixel(rays[i].x,rays[i].y,color);
                        } else {
                            rgb_t color = sphereRayPhong(center, inter, light, cam, s_color, P, a,b,c);
                            image.set_pixel(rays[i].x,rays[i].y,color);
                        }  
                    }
                }
            }
        }
}


int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(WIDTH, HEIGHT);
    rgb_t colormap = make_colour(85, 210, 100);
    colorBitmap(&image, &colormap);
    
    bitmap_image image2(WIDTH, HEIGHT);
    colorBitmap(&image2, &colormap);
    
    // create View
    Viewport view(glm::vec2(-5.0,-5.0),glm::vec2(5.0,5.0));
    float origin[3] = {0.0f,1.0f,0.0f};
    float gaze[3]   = {0.0f,0.0f,-1.0f};
    float up[3]     = {0.0f,1.0f,0.0f};
    float u[3];
    float v[3];
    float w[3] = {-gaze[0],
                  -gaze[1],
                  -gaze[2]};
    calculateU(w,up,u);
    calculateV(w,u,v);
    createRays(view,w,u,v,origin);

    // build world
    std::vector<Sphere_Mesh> world;
    float center[3] = {-3,0,-10};
    float color[3] = {128,0,0};
    world.push_back(*new Sphere_Mesh(20, center, 7.0, color));

    center[0] = 1; center[1] = 0; center[2] = -2;
    color[0] = 0; color[1] = 0; color[2] = 128;
    world.push_back(*new Sphere_Mesh(20, center, 0.5, color));
    
    center[0] = 2; center[1] = .5; center[2] = -2;
    color[0] = 128; color[1] = 50; color[2] = 15;
    world.push_back(*new Sphere_Mesh(10, center, .2, color));

    // render the world

    double t1 = omp_get_wtime();
    renderPerspWithPhong(image, world, origin);
    std::cout<<(omp_get_wtime() - t1)<<std::endl;
    
    image.save_image("persp.bmp");
    std::cout << "Success" << std::endl;
}


